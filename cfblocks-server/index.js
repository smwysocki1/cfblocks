"use strict";
const awsServerlessExpress = require("aws-serverless-express");
const { connect } = require("./app/mongoose");
let connection = null;
const binaryMimeTypes = [
  "application/octet-stream",
  "font/eot",
  "font/opentype",
  "font/otf",
  "image/jpeg",
  "image/png",
  "image/svg+xml"
];
exports.handler = async (event, context) =>{
  console.log(`EVENT: ${JSON.stringify(event)}`);
  context.callbackWaitsForEmptyEventLoop = false;

  if (connection === null) connection = await connect();
  const app = require("./app/server");
  const server = awsServerlessExpress.createServer(app, null, binaryMimeTypes);
  return awsServerlessExpress.proxy(server, event, context, "PROMISE").promise;
};
