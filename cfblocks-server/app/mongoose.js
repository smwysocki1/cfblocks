// mongoose.js

const mongoose = require("mongoose");

const uri = process.env.MONGODB_URL;

let connection;
const connect = async () => {
//   try {
//     connection = await mongoose.createConnection(uri, {
//       useNewUrlParser: true,
//       useFindAndModify: false,
//       useUnifiedTopology: true,
//       bufferCommands: false, // Disable mongoose buffering
//       bufferMaxEntries: 0, // and MongoDB driver buffering
//     });
//     return connection;
//   } catch (e) {
//     console.error("Could not connect to MongoDB...");
//     throw e;
//   }
  try {
      await mongoose
        .connect(process.env.MONGO_DB_URI, {
          useCreateIndex: true,
          useNewUrlParser: true,
          useFindAndModify: false,
          useUnifiedTopology: true,
          bufferCommands: false, // Disable mongoose buffering
          bufferMaxEntries: 0, // and MongoDB driver buffering
        });
        console.log("MongoDB connected");
    } catch(err) {
      console.error("Could not connect to MongoDB...");
      console.error(err);
      throw e;
    //   process.exit(0);
    }
};

function getConnection() {
  return connection;
}

module.exports = { connect, getConnection };
