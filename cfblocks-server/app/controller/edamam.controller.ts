import { Controller } from "./controller";
import { ErrorHandler } from "../common/service/errorHandler.service";
import { Error } from "../common/errors/errors";
import { EdamamService } from "../common/service/edamam.service";
import { FoodDatabaseClient, NutritionAnalysisClient, Measures } from 'edamam-api';
import { EdamamNutritionType, EdamamParserQueryParams, HealthLabel } from "../common/interface/edamam";
import { FoodService } from "../common/service/food.service";
 


export class EdamamController extends Controller {
  edamamFood: FoodDatabaseClient;
  edamamNutrients: NutritionAnalysisClient;
  edamamService: EdamamService;
  foodService: FoodService;
  constructor(app, errorHandler: ErrorHandler) {
    super(app, errorHandler, "/edamam");
    this.edamamFood = new FoodDatabaseClient({
      appId: process.env.edamamappid,
      appKey: process.env.edamamappkey
    });
    this.edamamNutrients = new NutritionAnalysisClient({
      appId: process.env.edamamnutrientid,
      appKey: process.env.edamamnutrientkey
    });
    this.edamamService = new EdamamService();
    this.foodService = new FoodService();
  }
  loadRoutes() {
    // this.router.get("/food/search", async (req, res, next) => {
    //   // { upc, query, nutritionType, health, calories, page, category, categoryLabel }
    //   try {
    //     const params = {} as EdamamParserQueryParams;
    //     if (req.query.q) {
    //       params.query = req.query.q
    //     } else if (req.query.upc) {
    //       params.UPC = req.query.upc;
    //     } else {
    //       throw Error.MISSING_UPC_OR_QUERY_PARAM;
    //     }
    //     if (req.query.filters) {
    //       const filters = req.query.filters;
    //       if (filters.health) {
    //         const healths = [];
    //         Object.keys(filters.health).forEach(health => {
    //           if (filters.health[health]) {
    //             healths.push(health);
    //           }
    //         });
    //         params.health = healths;
    //       }
    //     }
    //     // params['nutrition-type'] = req.query.nutritionType ? EdamamNutritionType.LOGGING : EdamamNutritionType.LOGGING;
    //     params['nutrition-type'] = req.query.nutritionType ? EdamamNutritionType.LOGGING : EdamamNutritionType.DEFAULT;
    //     const food = await this.edamamFood.search(params);
    //     // console.log(food);
    //     if (food) {
    //       await this.edamamService.cacheFood(food);
    //       res.json(food);
    //     } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
    //   } catch (error) {
    //     this.errorHandler.catchAllError(error, req, res, next);
    //   }
    // });


    this.router.post("/food/search", async (req, res, next) => {
      try {
        const food = await this.edamamService.search(req.body.q, req.body.filters.health, req.body.filters.diet, false);
        // console.log(food);
        if (food) {
          await this.edamamService.cacheFood(food);
          res.json(food);
        } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    })

    // this.router.post("/food/search", async (req, res, next) => {
    //   // { upc, query, nutritionType, health, calories, page, category, categoryLabel }
    //   try {
    //     const params = {} as any;
    //     if (req.body.q) {
    //       params.body = req.body.q
    //     } else if (req.body.upc) {
    //       params.UPC = req.body.upc;
    //     } else {
    //       throw Error.MISSING_UPC_OR_QUERY_PARAM;
    //     }
    //     if (req.body.filters) {
    //       const filters = req.body.filters;
    //       if (filters.health) {
    //         const healths = [];
    //         Object.keys(filters.health).forEach(health => {
    //           if (filters.health[health]) {
    //             healths.push(health);
    //           }
    //         });
    //         params.health = healths;
    //       }
    //     }
    //     // params['nutrition-type'] = req.body.nutritionType ? EdamamNutritionType.LOGGING : EdamamNutritionType.LOGGING;
    //     params['nutrition-type'] = req.body.nutritionType ? EdamamNutritionType.LOGGING : EdamamNutritionType.DEFAULT;
    //     console.log(params);
    //     const food = await this.edamamFood.search(params);
    //     // console.log(food);
    //     if (food) {
    //       await this.edamamService.cacheFood(food);
    //       res.json(food);
    //     } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
    //   } catch (error) {
    //     console.log(error);
    //     this.errorHandler.catchAllError(error, req, res, next);
    //   }
    // });

    this.router.post('/food/next_page', async (req, res, next) => {
      try { 
        const uri = req.body.uri;
        const nextPage = await this.edamamService.nextPage(uri);
        if (nextPage) {
          await this.edamamService.cacheFood(nextPage);
          res.send(nextPage);
        } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    })

    this.router.get('/food/nutrients', async (req, res, next) => {
      try {
        let cachedNutrients;
        if (req.query.cacheFirst) {
          cachedNutrients = await this.edamamService.getNutrientsByCache(req.query.foodId, parseInt(req.query.quantity), req.query.measure, req.query.qualifiers);
          if (cachedNutrients) {
            res.send(cachedNutrients);
          } 
        }

        if (!cachedNutrients) {
          // const nutrients = await this.edamamFood.getNutrients({ingredients: [{quantity: req.query.quantity, measure: req.query.measure, foodId: req.query.foodId}], servings: {}});
          const nutrients = await this.edamamService.getNutrients(req.query.foodId, parseInt(req.query.quantity), req.query.measure, req.query.qualifiers)
          // console.log(nutrients);
          if (nutrients) {
            await this.edamamService.cacheNutrients(req.query.foodId, nutrients, req.query.measure, parseInt(req.query.quantity), req.query.qualifiers);
            res.send(nutrients);
          } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
        }
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });

    this.router.get("/nutrients/search", async (req, res, next) => {
      try {
      // const food = await this.edamamNutrients.getNutritionData({ingredients: [{quantity: req.query.quantity, measure: Measures[req.query.measure], foodId: req.query.foodId}]});
      const food = await this.edamamNutrients.getNutritionData({ingredient: [{quantity: req.query.quantity, measure: req.query.measure, foodId: req.query.foodId, qualifiers: req.query.qualifiers}]});
      // console.log(food);
      if (food) {
        res.json(food);
      } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
    
    this.router.get('/food/byFoodId', async (req, res, next) => {
      try {
        let food;
        if (req.query.label) {
          food = await this.foodService.edamamFoodByIds([req.query.foodId]);
          food = food.find(f => f.food.label === req.query.label);
        } else {
          food = await this.foodService.edamamFoodById(req.params.foodId);
        }
        if(food) {
          res.json(food);
        } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });

    this.router.get('/foodId', async (req, res, next) => {
      try {
        const food = await this.edamamService.byId(req.query.foodId, parseInt(req.query.quantity), req.query.measure);
        if(food.data) {
          res.json(food.data);
        } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });this.router.get('/labels/caution', async (req, res, next) => {
      try {
        const cautionLabels = await this.edamamService.getCautionLabels();
        if(cautionLabels) {
          res.json(cautionLabels);
        } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
    this.router.get('/labels/diet', async (req, res, next) => {
      try {
        const dietLabels = await this.edamamService.getDietLabels();
        if(dietLabels) {
          res.json(dietLabels);
        } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
    this.router.get('/labels/health', async (req, res, next) => {
      try {
        const healthLabels = await this.edamamService.getHealthLabels();
        if(healthLabels) {
          res.json(healthLabels);
        } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
  }
}