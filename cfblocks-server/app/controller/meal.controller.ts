import { Controller } from "./controller";
import { MealService } from "../common/service/meal.service";
import { ErrorHandler } from "../common/service/errorHandler.service";
import { Error } from "../common/errors/errors";
import { MealDocument } from "../common/schema/Meal";

export class MealController extends Controller {
  meal: MealService;
  constructor(app, errorHandler: ErrorHandler) {
    super(app, errorHandler, "/meal");
    this.meal = new MealService();
  }
  loadRoutes() {
    this.router.get("/id/:id", async (req, res, next) => {
      try {
        const meal = await this.meal.getMealById(req.params.id);
        if (meal) {
          res.json(meal);
        } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
    this.router.get("/by/user/:user", async (req, res, next) => {
      try {
        const meal = await this.meal.getMeals(req.params.user, null, null);
        if (meal) {
          res.json(meal);
        } else this.errorHandler.catchAllError(Error.NOT_FOUND, req, res, next);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
    this.router.post("/create", async (req, res, next) => {
      try {
        const meal = await this.meal.create(req.body.user._id,req.body.foodId, req.body.label, req.body.type, req.body.eatenDate,
          req.body.eatenAmount, req.body.measure, req.body.qualifier);
        res.json(meal);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
    this.router.post("/update", async (req, res, next) => {
      try {
        const meal = await this.meal.update(req.body.user, req.body.meal as MealDocument);
        res.json(meal);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
    this.router.post("/update/mealType", async (req, res, next) => {
      try {
        const meal = await this.meal.updateMealType(req.body.meal, req.body.mealType);
        res.json(meal);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
    this.router.get("/delete/:id", async (req, res, next) => {
      try {
        const meal = await this.meal.delete(req.params.id);
        res.json(meal);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
    this.router.post("/calendar", async (req, res, next) => {
      try {
        // console.log(req.body.start, req.body.end);
        const meals = await this.meal.getMeals(req.body.user._id, req.body.start, req.body.end);
        // console.log(req.body.start, req.body.end, meals);
        const mealCalendars = this.meal.getMealCalendars(req.body.user._id, meals);
        // console.log(req.body.start, req.body.end, meals, mealCalendars);
        res.json(mealCalendars);
      } catch (error) {
        this.errorHandler.catchAllError(error, req, res, next);
      }
    });
  }
}
