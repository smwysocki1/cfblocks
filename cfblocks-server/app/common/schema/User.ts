// import * as mongoose from 'mongoose';
import { ObjectID } from 'mongodb';
import {Document, Model, model, Schema} from 'mongoose';
const { getConnection } = require("../../mongoose");
const conn = getConnection();

ObjectID.prototype.valueOf = function() {
  return this.toString();
};

export class IUser {
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  email: string;
  dob: Date;
  sex: string;
  bodyWeight: number;
  bodyWeightMetric: string;
  height: number;
  heightMetric: string;
  activityLevel: string;
  nutritionPlan: string;
  preferedCautionLabels: string[];
  preferedHealthLabels: string[];
  preferedDietLabels: string[];
  constructor(data: {
    username: string,
    password: string,
    firstName: string,
    lastName: string,
    email: string,
    dob?: Date,
    sex: string,
    bodyWeight: number,
    bodyWeightMetric: string,
    height: number,
    heightMetric: string,
    activityLevel: string,
    nutritionPlan: string,
    preferedCautionLabels: string[],
    preferedHealthLabels: string[],
    preferedDietLabels: string[]
  }) {
    this.username = data.username;
    this.password = data.password;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.email = data.email;
    this.dob = data.dob;
    this.sex = data.sex;
    this.bodyWeight = data.bodyWeight;
    this.bodyWeightMetric = data.bodyWeightMetric;
    this.height = data.height;
    this.heightMetric = data.heightMetric;
    this.activityLevel = data.activityLevel;
    this.nutritionPlan = data.nutritionPlan;
    this.preferedCautionLabels = data.preferedCautionLabels;
    this.preferedDietLabels = data.preferedDietLabels;
    this.preferedHealthLabels = data.preferedHealthLabels;
  }
  
  /* any method would be defined here*/
  // foo(): string {
  //   return this.name.uppercase() // whatever
  // }
}

const schema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  dob: {
    type: Date,
    required: true
  },
  sex: {
    type: String
  },
  bodyWeight: {
    type: Number
  },
  bodyWeightMetric: {
    type: String
  },
  height: {
    type: Number
  },
  heightMetric: {
    type: String
  },
  activityLevel: {
    type: String
  },
  nutritionPlan: {
    type: String
  },
  preferedCautionLabels: [{
    type: String
  }],
  preferedHealthLabels: [{
    type: String
  }],
  preferedDietLabels: [{
    type: String
  }]
});

// register each method at schema
// UserSchema.method('foo', User.prototype.foo)

export interface UserDocument extends IUser, Document { }
export const User: Model<UserDocument> = model<UserDocument>("User", schema, "User");
