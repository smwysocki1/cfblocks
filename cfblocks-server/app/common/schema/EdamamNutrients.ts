import * as mongoose from "mongoose";
import { ObjectID } from "mongodb";
import { Document, Model, model, Schema } from "mongoose";
import { UtilsService } from "../service/utils.service";
import { EdamamMeasure, IEdamamNutrients, IEdamamRecipe } from "../interface/edamam";
const { getConnection } = require("../../mongoose");
const conn = getConnection();

export interface EdamamNutrientsDocument extends IEdamamNutrients, Document {}

ObjectID.prototype.valueOf = function() {
  return this.toString();
};

const baseOptions = {
  collection: "EdamamNutrients", // the name of our collection
  timestamps: { createdAt: "created", updatedAt: "lastUpdated" }
};

const KcalLabel = new Schema({
  label: {
    type: String
  },
  quantity: {
    type: Number
  },
  unit: {
    type: String
  }
});

// Our Base schema: these properties will be shared with our "real" schemas
const EdamamNutrientsBase = new mongoose.Schema(
  {
    foodId: {
      type: String,
      required: true
    },
    measure: {
      uri: {
        type: String,
        required: true
      },
      label: {
        type: String,
        required: true
      },
      weight: {
        type: Number
      }
    },
    nutrients: {
      uri: {
        type: String,
        required: true
      },
      calories: {
        type: Number,
        required: true
      },
      totalWeight: {
        type: Number,
        required: true
      },
      dietLabels: [{
        type: String
      }],
      healthLabels: [{
        type: String
      }],
      cautions: [{
        type: String
      }],
      totalNutrients : {
        ENERC_KCAL: {
          label: {
            type: String
          },
          quantity: {
            type: Number
          },
          unit: {
            type: String
          }
        },
        FAT: KcalLabel,
        FASAT: KcalLabel,
        FAMS: KcalLabel,
        FAPU: KcalLabel,
        CHOCDF: KcalLabel,
        FIBTG: KcalLabel,
        SUGAR: KcalLabel,
        PROCNT: KcalLabel,
        CHOLE: KcalLabel,
        NA: KcalLabel,
        CA: KcalLabel,
        MG: KcalLabel,
        K: KcalLabel,
        FE: KcalLabel,
        ZN: KcalLabel,
        P: KcalLabel,
        VITA_RAE: KcalLabel,
        VITC: KcalLabel,
        THIA: KcalLabel,
        RIBF: KcalLabel,
        NIA: KcalLabel,
        VITB6A: KcalLabel,
        FOLDFE: KcalLabel,
        FOLFD: KcalLabel,
        FOLAC: KcalLabel,
        VITB12: KcalLabel,
        VITD: KcalLabel,
        TOCPHA: KcalLabel,
        VITK1: KcalLabel,
        WATER: KcalLabel
      },
      totalDaily: {
        ENERC_KCAL: KcalLabel,
        FAT: KcalLabel,
        FASAT: KcalLabel,
        CHOCDF: KcalLabel,
        FIBTG: KcalLabel,
        PROCNT: KcalLabel,
        CHOLE: KcalLabel,
        NA: KcalLabel,
        CA: KcalLabel,
        MG: KcalLabel,
        K: KcalLabel,
        FE: KcalLabel,
        ZN: KcalLabel,
        P: KcalLabel,
        VITA_RAE: KcalLabel,
        VITC: KcalLabel,
        THIA: KcalLabel,
        RIBF: KcalLabel,
        NIA: KcalLabel,
        VITB6A: KcalLabel,
        FOLDFE: KcalLabel,
        VITB12: KcalLabel,
        VITD: KcalLabel,
        TOCPHA: KcalLabel,
        VITK1: KcalLabel,
      },
      ingredients: [
        {
          parsed: [
            {
              quantity: {
                type: Number
              },
              measure: {
                type: String
              },
              food: {
                type: String
              },
              foodId: {
                type: String
              },
              weight: {
                type: Number
              },
              retainedWeight: {
                type: String
              },
              measureURI: {
                type: String
              },
              status: {
                type: String
              }
            }
          ]
        }
      ]
    },
    quantity: {
      type: Number,
      required: true
    },
    qualifier: {
      uri: {
        type: String
      },
      label: {
        type: String
      },
      weight: {
        type: Number
      }
    }
  }, baseOptions
);

export const EdamamNutrients: Model<EdamamNutrientsDocument> = model<EdamamNutrientsDocument>("EdamamNutrients", EdamamNutrientsBase, "EdamamNutrients");

// module.exports = model<EdamamNutrientsDocument>("EdamamNutrients");



// export const Recipe: Model<EdamamNutrientsDocument> = EdamamNutrientsBase.discriminator<EdamamNutrientsDocument>(
//   "Recipe",
//   recipeSchema
// );
