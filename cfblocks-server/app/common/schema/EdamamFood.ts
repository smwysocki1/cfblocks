import * as mongoose from "mongoose";
import { ObjectID } from "mongodb";
import { Document, Model, model, Schema } from "mongoose";
import { UtilsService } from "../service/utils.service";
import { FoodCalcService } from "../service/food-calc.service";
import { EdamamMeasure, IEdamamFood, IEdamamRecipe } from "../interface/edamam";
const { getConnection } = require("../../mongoose");
const conn = getConnection();

export interface EdamamFoodDocument extends IEdamamFood, Document {}

ObjectID.prototype.valueOf = function() {
  return this.toString();
};

const baseOptions = {
  collection: "EdamamFood", // the name of our collection
  timestamps: { createdAt: "created", updatedAt: "lastUpdated" }
};

// Our Base schema: these properties will be shared with our "real" schemas
const EdamamFoodBase = new Schema(
  {
    foodId: {
      type: String,
      required: true
    },
    food: {
      foodId: {
        type: String,
        required: true
      },
      label: {
        type: String,
        required: true
      },
      nutrients: {
        ENERC_KCAL: {
          type: Number
        },
          PROCNT: {
          type: Number
        },
          FAT: {
          type: Number
        },
          CHOCDF: {
          type: Number
        },
          FIBTG: {
          type: Number
        }
      },
      category: {
        type: String,
        enum: ['Generic foods','Packaged foods','Generic meals','Fast foods']
      },
      categoryLabel: {
        type: String,
        enum: ['food', 'recipe', 'meal']
      },
      foodContentsLabel: {
        type: String
      },
      image: {
        type: String
      },
    },
    recipe: {
      foodId: {
        type: String
      },
      label: {
        type: String
      },
      nutrients: {
        ENERC_KCAL: {
          type: Number
        },
          PROCNT: {
          type: Number
        },
          FAT: {
          type: Number
        },
          CHOCDF: {
          type: Number
        },
          FIBTG: {
          type: Number
        }
      },
      category: {
        type: String,
        enum: ['Generic foods','Packaged foods','Generic meals','Fast foods']
      },
      categoryLabel: {
        type: String,
        enum: ['food', 'recipe']
      },
      foodContentsLabel: {
        type: String
      },
      image: {
        type: String
      },
    },
    measures: [{
      uri: {
        type: String,
        required: true
      },
      label: {
        type: String,
        required: true
      }
    }]
  }, baseOptions
);

export const EdamamFood: Model<EdamamFoodDocument> = model<EdamamFoodDocument>("EdamamFood", EdamamFoodBase, "EdamamFood");

// module.exports = model<EdamamFoodDocument>("EdamamFood");

// export const Recipe: Model<EdamamFoodDocument> = EdamamFoodBase.discriminator<EdamamFoodDocument>(
//   "Recipe",
//   recipeSchema
// );
