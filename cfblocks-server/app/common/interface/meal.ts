import { EdamamMeasure, MealType } from "./edamam";
import * as moment from "moment-timezone";
moment.tz.setDefault('Etc/UTC');

export class IMeal {
  eatenBy: string;
  eatenDate: Date = moment().toDate();
  foodId: string;
  label: string;
  eatenAmount: number;
  measure: EdamamMeasure;
  qualifier?: EdamamMeasure;
  type: MealType;
  constructor(data: {
    eatenBy: string,
    eatenDate: Date,
    label: string,
    foodId: string,
    eatenAmount: number,
    measure: EdamamMeasure,
    qualifier: EdamamMeasure,
    type: MealType
  }) {
    this.eatenBy = data.eatenBy;
    this.eatenDate = data.eatenDate;
    this.foodId = data.foodId;
    this.label = data.label;
    this.eatenAmount = data.eatenAmount;
    this.measure = data.measure;
    if (data.qualifier) {
      this.qualifier = data.qualifier;
    }
    this.type = data.type;
  }
}
