import { Meal, MealDocument } from "../schema/Meal";
import { Error, ErrorObject } from "../errors/errors";
import { FoodService } from "./food.service";
import { IMeal } from "../interface/meal";
import * as moment from 'moment-timezone';
import { MealType } from "../interface/edamam";
import { IUser } from "../schema/User";

export class MealService {
  foodService: FoodService = new FoodService();
  constructor() {}
  async getMealById(_id: string): Promise<MealDocument> {
    if (_id) {
      return (await Meal.findById(_id).exec()) as MealDocument;
    } else return null;
  }
  async getMeals(
    user?: string,
    startDate?: Date,
    endDate?: Date
  ): Promise<IMeal[]> {
    let query = Meal.find();
    if (user) {
      query = query.find({ eatenBy: user });
    }
    if (startDate) {
      query = query.find({ eatenDate: { $gte: startDate } });
    }
    if (endDate) {
      query = query.find({ eatenDate: { $lte: endDate } });
    }
    // console.log(startDate, endDate, query);
    const result = await query
      // .populate({
      //   path: "food",
      //   populate: {
      //     path: "ingredients.food",
      //     model: "Food"
      //   }
      // })
      .exec();
    return result.map(meal => {
      return meal.toJSON() as IMeal;
    })
  }
  async create(user: string, foodId: string, label: string, type: MealType, when: Date, quantity: number, measurement: string, qualifiedQualifier?: string): Promise<MealDocument> {
    if (!user) {
      throw Error.USERNAME_NOT_FOUND;
    } else if (!foodId){
      throw Error.FOOD_NOT_SPECIFIED;
    } else if (!label){
      throw Error.FOOD_LABEL_NOT_SPECIFIED;
    } else if (!when) {
      throw Error.DATE_NOT_SPECIFIED;
    } else if (quantity === undefined) {
      throw Error.NO_QUANTITY_SPECIFIED;
    } else if (!measurement) {
      throw Error.NO_MEASUREMENT_SPECIFIED;
    } else if (!type) {
      throw Error.INVALID_MEAL_TYPE;
    }
    const food = await this.foodService.edamamFoodById(foodId);
    const measure = food.measures.find(m => m.uri === measurement);
    let qualifier;
    if (qualifiedQualifier && measure.qualified && measure.qualified[0] && measure.qualified[0].qualifiers && measure.qualified[0].qualifiers.length > 0) {
      qualifier = measure.qualified[0].qualifiers.find(q => q.uri === qualifiedQualifier);
      qualifier.weight = measure.qualified[0].weight;
    }
    // const nutrients = this.foodService.getNutrient(foodId, measure, quantity, qualifier);
    const newMeal = new IMeal({
      eatenBy: user,
      eatenDate: when,
      foodId,
      type,
      label,
      eatenAmount: quantity,
      measure,
      qualifier
    });
    return await Meal.create(newMeal) as MealDocument;


    // if (meal) {
    //   if (meal.food || meal.recipe) {
    //     const mealExists: boolean = await this.mealExists(meal);
    //     if (!mealExists) {
    //       const newMeal = await new Meal(meal);
    //       return (await Meal.create(newMeal)) as MealDocument;
    //     }
    //     throw Error.MEAL_ALREADY_EXISTS;
    //   } else throw Error.INVALID_MEAL;
    // } else return null;
  }
  async update(user: IUser, meal: any): Promise<MealDocument> {
    if (meal) {
      return (await Meal.findByIdAndUpdate(
        meal.id,
        { $set: { ...meal } },
        { new: true }
      ).exec()) as MealDocument;
    } else return null;
  }
  async updateMealType(meal: any, mealType: MealType): Promise<IMeal> {
    if (meal) {
      return (await Meal.findOneAndUpdate(
        {_id: meal.id},
        { $set: { type: mealType } }
      ).exec()) as IMeal;
    } else return null;
  }
  async delete(_id: string): Promise<MealDocument> {
    if (_id) {
      return (await Meal.findByIdAndDelete(_id).exec()) as MealDocument;
    } else return null;
  }
  async mealExists(meal: MealDocument): Promise<boolean> {
    if (meal) {
      const mealFound = await this.getMealById(meal._id);
      return !!mealFound;
    } else return false;
  }
  getMealCalendars(user: string, meals: IMeal[]): {user: string,date: Date,meals: IMeal[]}[] {
    if (meals && meals.length > 0) {
      meals = meals.sort((a,b) => {return moment(a.eatenDate).isBefore(moment(b.eatenDate)) ? -1 : 1}) as IMeal[];
      let result:{user: string,date: Date,meals: IMeal[]}[] = [] as {user: string,date: Date,meals: IMeal[]}[];
      let currentDate = moment(meals[0].eatenDate).startOf('day');
      meals.forEach(meal => {
        while(!currentDate.isAfter(meal.eatenDate, 'day')) {
          result.push({date: currentDate.toDate(),meals:[], user});
          currentDate = currentDate.add(1, 'day');
        }
        const mealCalendar = result.find(r => moment(r.date).isSame(moment(meal.eatenDate), 'day'));
        mealCalendar.meals.push(meal);
      });
      return result;
    } else return [];
  }
}
