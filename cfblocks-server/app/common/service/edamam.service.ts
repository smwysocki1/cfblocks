const axios = require("axios");

import { FoodDatabaseClient, NutritionAnalysisClient, Measures } from 'edamam-api';
import { DietLabel, EdamamCaution, EdamamFood, EdamamFoodRes, EdamamMeasure, EdamamNutrientsRes, HealthLabel, IEdamamFood, IEdamamNutrients, KcalLabel } from '../interface/edamam';
import { EdamamNutrients, EdamamNutrientsDocument } from '../schema/EdamamNutrients';
import { FoodService } from './food.service';
const dietLabels: DietLabel[] = require('../../data/dietLabel.json');
const healthLabels: HealthLabel[] = require('../../data/healthLabel.json');

// const dietLabels: DietLabel[] = JSON.parse(fs.readFileSync('../../../data/dietLabel.json', 'utf8'));
// const healthLabels: HealthLabel[] = JSON.parse(fs.readFileSync('../../data/healthLabel.json', 'utf8'));
export class EdamamService {
    foodService: FoodService = new FoodService();
    constructor(){
    }
    async search(q: string, health: any[], diet: any[], foodLogging: boolean): Promise<EdamamFoodRes> {
        // let uri = `https://api.edamam.com/search?app_id=${process.env.edamamappid}&app_key=${process.env.edamamappkey}&nutrition-type=${foodLogging ? 'logging' : 'cooking'}`;
        let uri = `https://api.edamam.com/api/food-database/v2/parser?app_id=${process.env.edamamappid}&app_key=${process.env.edamamappkey}&nutrition-type=${foodLogging ? 'logging' : 'cooking'}`;
        if(!q) {
            throw new Error('No Search Query provided.');
        }
        if (q) {
            uri += `&ingr=${encodeURIComponent(q)}`;
        }
        if (health && Object.keys(health).length > 0) {
            Object.keys(health).forEach((h: string) => {
                if (health[h]) {
                    const l = healthLabels.find((label: HealthLabel) => label['web-label'] === h);
                    uri += `&health=${l['api-parameter']}`;
                }
            });
        }
        if (diet && Object.keys(diet).length > 0) {
            Object.keys(diet).forEach((d: string) => {
                if (diet[d]) {
                    const l = dietLabels.find((label: DietLabel) => label['web-label'] === d);
                    uri += `&diet=${l['api-parameter']}`;
                }
            });
        }
        // if (upc) {
        //     upc += `&upc=${encodeURIComponent(upc)}`;
        // }
        const res = await axios.get(uri, {headers: {"Accept": "application/json"}})
        return res.data as EdamamFoodRes;
    }
    async nextPage(uri: string): Promise<EdamamFoodRes> {
        const res = await axios.get(uri);
        return res.data;
    }
    async getNutrients(foodId: string, quantity: number, measure: string, qualifiers?: string): Promise<EdamamNutrientsRes> {
        let uri = `https://api.edamam.com/api/food-database/v2/nutrients?app_id=${process.env.edamamappid}&app_key=${process.env.edamamappkey}`
        const food = {
            foodId, quantity, measureURI: measure
        } as any;
        if (food) {
            food.qualifiers = [qualifiers];
        }
        const body = {
            ingredients: [food]
        };
        const res = await axios.post(uri, body, {headers: {"Content-Type": "application/json"}});
        return res.data as EdamamNutrientsRes;
    }
    async getNutrientsByCache(foodId: string, quantity: number, measurement: string, qualifiedQualifier?: string): Promise<EdamamNutrientsRes> {
        const food = await this.foodService.edamamFoodById(foodId);
        const measure = food.measures.find(m => m.uri === measurement);
        let qualifier;
        if (qualifiedQualifier && measure.qualified && measure.qualified[0] && measure.qualified[0].qualifiers && measure.qualified[0].qualifiers.length > 0) {
            qualifier = measure.qualified[0].qualifiers.find(q => q.uri === qualifiedQualifier);
            qualifier.weight = measure.qualified[0].weight;
        }
        let nutrientFound = await this.foodService.getNutrient(foodId, measure, 1, qualifier);
        if(nutrientFound) {
            const nutrients = nutrientFound.nutrients as EdamamNutrientsRes;
            if (nutrients.calories)
                nutrients.calories *= quantity;
            if (nutrients.totalWeight)        
                nutrients.totalWeight *= quantity;
            if (nutrients.totalNutrients && Object.keys(nutrients.totalNutrients).length > 0) {
                Object.keys(nutrients.totalNutrients).forEach(tn  => {
                    nutrients.totalNutrients[tn].quantity *= quantity
                });
            }
            if (nutrients.totalDaily && Object.keys(nutrients.totalDaily).length > 0) {
                Object.keys(nutrients.totalDaily).forEach(td => {
                    nutrients.totalDaily[td].quantity *= quantity
                });
            }
            if (nutrients.ingredients && nutrients.ingredients.length > 0) {
                nutrients.ingredients.forEach(ingredent => {
                    if (ingredent.parsed && ingredent.parsed.length > 0) {
                        ingredent.parsed.forEach(parsed => {
                            if (parsed.quantity)
                                parsed.quantity *= quantity;
                            if (parsed.weight)
                                parsed.weight *= quantity;
                        })
                    }
                });
            }
            return nutrients;
        } else {
            return undefined;
        }
    }

    async byId(id: string, quantity: number, measure: string, foodLogging?: boolean): Promise<any> {
        let uri = `https://api.edamam.com/api/food-database/v2/nutrients?nutrition-type=${foodLogging ? 'logging' : null}&app_id=${process.env.edamamnutrientid}&app_key=${process.env.edamamnutrientkey}`;
        const params = {ingredients: [{foodId: id, quantity: quantity, measureURI: Measures[measure.toLowerCase()]}]};
        return axios.post(uri, params, {headers: {"Content-Type": "application/json"}});
        // return axios.post(uri, {ingredents: [{foodId: id, quantity, measure: Measures[measure.toLowerCase()]}]}, {headers: {"Content-Type": "application/json"}});
    }

    async cacheFood(food: EdamamFoodRes): Promise<any[]> {
        let foodIds = [];
        food.parsed.forEach(parsed => {
            if (parsed.food && parsed.food.foodId){
                foodIds.push(parsed.food.foodId);
            }
        })
        const hintsFoodIds = food.hints.map(f => {
            if (f.food) {
                return f.food.foodId;
            } else if (f.recipe) {
                return f.recipe.foodId;
            } else return undefined;
        });
        foodIds = foodIds.concat(hintsFoodIds);
        const preCachedFoods = await this.foodService.edamamFoodByIds(foodIds);

        const foodIdsToCache = foodIds.filter(fid => !preCachedFoods.some(cachedFood => cachedFood.foodId === fid));

        //TODO Precached Foods may not have shared their measurements

        return Promise.all(foodIdsToCache.map(async foodId => {
            let edamamFood: EdamamFood;
            let edamamMeasures: EdamamMeasure[] = this.getMeasures(food, foodId);
            if (food.parsed && food.parsed[0] && food.parsed[0].food && foodId === food.parsed[0].food.foodId) {
                edamamFood = food.parsed[0].food;
            } else {
                const hint = food.hints.find(f => f.food && f.food.foodId === foodId);
                edamamFood = hint.food;
            }
            return await this.foodService.cacheEdamamFood(foodId, edamamFood, edamamMeasures);
        }))
    }

    getMeasures(food: EdamamFoodRes, foodId: string): EdamamMeasure[] {
        let measures = []

        if (food.parsed) {
            food.parsed.forEach(parsed => {
                if (!measures.some(m=> m.uri === 'http://www.edamam.com/ontologies/edamam.owl#Measure_gram') || parsed.food.foodId === foodId) {
                    measures.push({uri: 'http://www.edamam.com/ontologies/edamam.owl#Measure_gram', label: 'Gram'});
                }
            })
        }
        food.hints.filter(hint => hint.food && hint.food.foodId === foodId).forEach(foodHint => {
            if (foodHint.measures) {
                measures = measures.concat(foodHint.measures.filter(m=> !measures.some(measure => measure.uri === m.uri)));
            }
        });

        return measures.filter(m => m.uri !== 'http://www.edamam.com/ontologies/edamam.owl#Measure_default');
    }

    async cacheNutrients(foodId: string, nutrients: EdamamNutrientsRes, measurement: string, quantity: number, qualifiedQualifier?: string): Promise<any> {
        if ((quantity == 100 && measurement === 'http://www.edamam.com/ontologies/edamam.owl#Measure_gram') || quantity === 1) {
            const food = await this.foodService.edamamFoodById(foodId);
            const measure = food.measures.find(m => m.uri === measurement);
            const nutrient:any = {foodId, nutrients, measure};
            if (qualifiedQualifier && measure.qualified && measure.qualified[0] && measure.qualified[0].qualifiers && measure.qualified[0].qualifiers.length > 0) {
              nutrient.qualifier = measure.qualified[0].qualifiers.find(q => q.uri === qualifiedQualifier);
              nutrient.qualifier.weight = measure.qualified[0].weight;
            }
            const nutrientFound = this.foodService.getNutrient(nutrient.foodId, nutrient.measure, quantity, nutrient.qualifier);
            if (!nutrientFound) {
                return await this.foodService.cacheNutrients(nutrient.foodId, nutrient.nutrients, nutrient.measure, quantity, nutrient.qualifier);
            } else {
                return undefined;
            }
        } else {
            const food = await this.foodService.edamamFoodById(foodId);
            const measure = food.measures.find(m => m.uri === measurement);
            const nutrient:any = {foodId, nutrients, measure};
            if (qualifiedQualifier && measure.qualified && measure.qualified[0] && measure.qualified[0].qualifiers && measure.qualified[0].qualifiers.length > 0) {
              nutrient.qualifier = measure.qualified[0].qualifiers.find(q => q.uri === qualifiedQualifier);
              nutrient.qualifier.weight = measure.qualified[0].weight;
            }
            const nutrientFound = await this.foodService.getNutrient(nutrient.foodId, nutrient.measure, 1, nutrient.qualifier);
            if (!nutrientFound) {
                nutrient.nutrients = await this.getNutrients(nutrient.foodId, 1, nutrient.measure.uri, nutrient.qualifier);
                return await this.foodService.cacheNutrients(nutrient.foodId, nutrient.nutrients, nutrient.measure, 1, nutrient.qualifier);
            } else {
                return undefined;
            }
        }
    }
    async getCautionLabels(): Promise<EdamamCaution[]> {
        const nutrients = await EdamamNutrients.find({},{nutrients:{cautions:1}}).exec();
        return nutrients.reduce((result, next: EdamamNutrientsDocument) => {
            if (next) {
                const edamamNutrients = next.toJSON() as IEdamamNutrients;
                if (edamamNutrients && edamamNutrients.nutrients && edamamNutrients.nutrients.cautions) {
                    edamamNutrients.nutrients.cautions.forEach(caution => {
                        if(!result.some(c => c === caution)) {
                            result.push(caution);
                        }
                    });
                }
            }
            return result;
        }, [])
    }
    async getDietLabels(): Promise<DietLabel[]> {
        const nutrients = await EdamamNutrients.find({},{nutrients:{dietLabels:1}}).exec();
        return nutrients.reduce((result, next: EdamamNutrientsDocument) => {
            if (next) {
                const edamamNutrients = next.toJSON() as IEdamamNutrients;
                if (edamamNutrients && edamamNutrients.nutrients && edamamNutrients.nutrients.dietLabels) {
                    edamamNutrients.nutrients.dietLabels.forEach(dietLabel => {
                        if(!result.some(dl => dl === dietLabel)) {
                            result.push(dietLabel);
                        }
                    });
                }
            }
            return result;
        }, [])
    }
    async getHealthLabels(): Promise<HealthLabel[]> {
        const nutrients = await EdamamNutrients.find({},{nutrients:{healthLabels:1}}).exec();
        return nutrients.reduce((result, next: EdamamNutrientsDocument) => {
            if (next) {
                const edamamNutrients = next.toJSON() as IEdamamNutrients;
                if (edamamNutrients && edamamNutrients.nutrients && edamamNutrients.nutrients.healthLabels) {
                    edamamNutrients.nutrients.healthLabels.forEach(healthLabel => {
                        if(!result.some(hl => hl === healthLabel)) {
                            result.push(healthLabel);
                        }
                    });
                }
            }
            return result;
        }, [])
    }
}