import {Injectable} from "@angular/core";
import {Food, MealFood} from '../models/meal.module';
import {User} from '../models/user.model';
import { UtilService } from "./util.service";

@Injectable()
export class BlockCalculatorService {
  constructor(private utils: UtilService){}
  getGoals(user: User): {calories: number, carbs: number, fats: number, protein: number} {
    const goals = {calories: undefined, carbs: undefined, fats: undefined, protein: undefined};
    if (user) {
      let BMI = 0;
      if (user && user.bodyWeight && user.bodyWeightMetric && user.height  && user.heightMetric) {
        BMI = this.calculateBMI(
          user.sex, 
          this.utils.userWeightKG(user.bodyWeight, user.bodyWeightMetric), 
          this.utils.userHeightCM(0, user.height, user.heightMetric),
          this.utils.getUserAge(user.dob)
        );
      }
      let BMR = 0;
      if (BMI > 0) {
        BMR = this.calculateBMR(
          user.sex, 
          this.utils.userWeightKG(user.bodyWeight, user.bodyWeightMetric), 
          this.utils.userHeightCM(0, user.height, user.heightMetric),
          this.utils.getUserAge(user.dob),
          user.activityLevel);
      }
      if (BMR > 0 && user.nutritionPlan) {
        switch(user.nutritionPlan) {
          case 'calorie-tracking':
            goals.calories = BMR;
            break;
          case 'zone':
            goals.calories = BMR;
            goals.carbs = (BMR * .4) / 4; // 40% & 4cals per gram
            goals.fats = (BMR * .3) / 9; // 40% & 4cals per gram
            goals.protein = (BMR * .3) / 4; // 40% & 4cals per gram
            break;
          case 'zone-block':
            goals.calories = BMR;
            goals.carbs = (BMR * .4) / 4; // 40% & 4cals per gram
            goals.fats = (BMR * .3) / 9; // 40% & 4cals per gram
            goals.protein = (BMR * .3) / 4; // 40% & 4cals per gram
            break;
          case 'keto':
            goals.calories = BMR;
            goals.carbs = (BMR * .1) / 4; // 40% & 4cals per gram
            goals.fats = (BMR * .7) / 9; // 40% & 4cals per gram
            goals.protein = (BMR * .2) / 4; // 40% & 4cals per gram
            break;
          default: 
            goals.calories = BMR;
            goals.carbs = (BMR * .4) / 4; // 40% & 4cals per gram
            goals.fats = (BMR * .3) / 9; // 40% & 4cals per gram
            goals.protein = (BMR * .3) / 4; // 40% & 4cals per gram
            break;
        }
      }
    }
    return goals;
    // return {calories: 1000, carbs: 250, fats: 50, protein: 100};
  }

  calculateBMI(sex: string, weightInKG: number, heightInCm: number, ageInYears: number): number {
    if (sex && weightInKG > 0 && heightInCm > 0 && ageInYears >= 0) {
      if (sex === 'male') {
        return 66.74 + 
        (13.75 * weightInKG) + 
        (5.003 * heightInCm) - 
        (6.755 * ageInYears);
      } else if (sex === 'female') {
        return 655.1 + 
        (9.563 * weightInKG) + 
        (1.85 * heightInCm) - 
        (4.676 * ageInYears);
      } else {
        return 0;
      }
    } else return 0;
  }
  calculateBMR(sex: string, weightInKG: number, heightInCm: number, ageInYears: number, activityLevel: string): number {
    if (activityLevel) {
      switch(activityLevel) {
        case 'no-exercise': return this.calculateBMI(sex, weightInKG, heightInCm, ageInYears) * 1.2;
        case 'little-exercise': return this.calculateBMI(sex, weightInKG, heightInCm, ageInYears) * 1.375;
        case 'moderate-exercise': return this.calculateBMI(sex, weightInKG, heightInCm, ageInYears) * 1.55;
        case 'heavy-exercise': return this.calculateBMI(sex, weightInKG, heightInCm, ageInYears) * 1.725;
        case 'extra-exercise': return this.calculateBMI(sex, weightInKG, heightInCm, ageInYears) * 1.9;
      }
    }
    return 0;
  }
  getBlocks(user: User): number {
    if (user && user.username) {
      let lbm = user.body.lbm.weight;
      if (user.body.lbm.metric.toLowerCase() === 'kilograms') {
        lbm = 2.2 * lbm;
      }
      const activityLevel = user.lifeStyle.activityLevel;
      const goals = user.lifeStyle.fitnessGoal;
      let protein = 0;
      if (activityLevel === 'Seated Most of the Day') {
        protein = lbm * .65;
      } else if (activityLevel === 'Regularly On/Off Feet Throughout the Day') {
        protein = lbm * .7;
      } else if (activityLevel === 'On Feet and Moving Consistently Moving All Day') {
        protein = lbm * .8;
      } else if (activityLevel === 'Professional Athlete') {
        protein = lbm * .85;
      }
      let blocks = protein / 7;
      if (goals === 'Lose Weight') {
        blocks = blocks - 1;
      } else if (goals === 'Gain Mass' || goals === 'Athletic Performance') {
        blocks = blocks + 1;
      }
      return blocks;
    } else {
      return 0;
    }
  }
  getFoodMealCarbs(food: MealFood) {
    if (food && food.food && food.food.serving) {
      return (food.servingAmount / food.food.serving.amount) * food.food.carb;
    } else {
      return 0;
    }
  }
  getFoodMealFats(food: MealFood) {
    if (food && food.food && food.food.serving) {
      return (food.servingAmount / food.food.serving.amount) * food.food.fat;
    } else {
      return 0;
    }
  }
  getFoodMealProtein(food: MealFood) {
      if (food && food.food && food.food.serving) {
        return (food.servingAmount / food.food.serving.amount) * food.food.protein;
      } else {
        return 0;
      }
  }
  calcCarbs(food: MealFood): number {
      if (food && food.food && food.food.serving) {
        return this.getFoodMealCarbs(food);
      } else {
        return 0;
      }
  }
  calcFats(food: MealFood): number {
    if (food && food.food && food.food.serving) {
      return this.getFoodMealFats(food);
    } else {
      return 0;
    }
  }
  calcProtein(food: MealFood): number {
    if (food && food.food && food.food.serving) {
      return this.getFoodMealProtein(food);
    } else {
      return 0;
    }
  }
  calcCalories(food: MealFood): number {
    if (food && food.food && food.food.serving) {
      return this.carbsToCals(this.calcCarbs(food)) + this.fatsToCals(this.calcFats(food)) + this.proteinToCals(this.calcProtein(food));
    } else {
      return 0;
    }
  }
  calcFoodCalories(food: Food): number {
    const mealFood = new MealFood();
    mealFood.food = food;
    mealFood.servingAmount = food.serving.amount;
    return this.calcCalories(mealFood);
  }
  getCarbs(blocks: number) { // TODO needs to reflect template
    return blocks * 9;
  }
  getFats(blocks: number) {
    return blocks * 1.5;
  }
  getProtein(blocks: number) {
    return blocks * 7;
  }
  carbsToCals(carbs: number) {
    return carbs * 4;
  }
  fatsToCals(fats: number) {
    return fats * 9;
  }
  proteinToCals(protein: number) {
    return protein * 4;
  }
  dailyCals(user: User) {
    if (user && user.username) {
      if (user.blockTemplate.metric === 'Zone Block') {
        const blocks = this.getBlocks(user);
        let cals = 0;
        cals += this.carbsToCals(blocks * user.blockTemplate.carbs);
        cals += this.fatsToCals(blocks * user.blockTemplate.fats);
        cals += this.proteinToCals(blocks * user.blockTemplate.protein);
        return cals;
      } else if (user.blockTemplate.metric === 'Percentage') {
        let lbm = user.body.lbm.weight;
        if (user.body.lbm.metric.toLowerCase() === 'kilograms') {
          lbm = 2.2 * lbm;
        }
        const activityLevel = user.lifeStyle.activityLevel;
        let protein = 0;
        if (activityLevel === 'Seated Most of the Day') {
          protein = lbm * .65;
        } else if (activityLevel === 'Regularly On/Off Feet Throughout the Day') {
          protein = lbm * .7;
        } else if (activityLevel === 'On Feet and Moving Consistently Moving All Day') {
          protein = lbm * .8;
        } else if (activityLevel === 'Professional Athlete') {
          protein = lbm * .85;
        }
        const baseFigure = protein / 30;
        let cals = 0;
        cals += this.carbsToCals(baseFigure * user.blockTemplate.carbs);
        cals += this.fatsToCals(baseFigure * user.blockTemplate.fats);
        cals += this.proteinToCals(baseFigure * user.blockTemplate.protein);
        return cals;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }
  dailyCarbs(user: User) {
    if (user && user.username) {
      return this.dailyCals(user) / 100 * user.blockTemplate.carbs / 4;
    } else {
      return 0;
    }
  }
  dailyFats(user: User) {
    if (user && user.username) {
      return this.dailyCals(user) / 100 * user.blockTemplate.fats / 9;
    } else {
      return 0;
    }
  }
  dailyProtein(user: User) {
    if (user && user.username) {
      return (this.dailyCals(user) / 100 * user.blockTemplate.protein) / 7;
    } else {
      return 0;
    }
  }
}
