import {Injectable} from '@angular/core';
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');
import { Food, Meal, MealCalendar } from '../models/meal.module';
import { User } from '../models/user.model';
import { LoginService } from './login.service';
import {RequestService} from './request.service';
@Injectable()
export class FoodAPIService {
    constructor(private request: RequestService, private ls: LoginService){}
    getAllFood() {
        const isadmin = this.ls.isAdmin()
        return this.request.get('/food/all' + isadmin ? '?isAdmin=TRUE' : '');
    }
    getFood(id: string) {
        return this.request.get('/food/byId/'+id);
    }
    searchFood(q: string) {
        return this.request.get('/food/search/'+q);
    }
    createFood(food: Food) {
        const user = this.ls.getUser();
        return this.request.post('/food/create', {food,user});
    }
    updateFood(food) {
        const user = this.ls.getUser();
        return this.request.post('/food/update', {food, user});
    }
    favoriteFood(food:Food) {
        const user = this.ls.getUser();
        return this.request.post('/food/favorite', {food, user});
    }
    unFavoriteFood(food:Food) {
        const user = this.ls.getUser();
        return this.request.post('/food/unfavorite', {food, user});
    }
    favoriteMeal(meal:Meal) {
        const user = this.ls.getUser();
        return this.request.post('/meal/favorite', {meal, user});
    }
    unFavoriteMeal(meal:Meal) {
        const user = this.ls.getUser();
        return this.request.post('/meal/unfavorite', {meal, user});
    }
    saveMealCalendar(mealCalendar: MealCalendar) {
        const user = this.ls.getUser();
        return this.request.post('/mealCalendar/save', {mealCalendar, user});
    }
    getMealCalendarByDateRange(start: Date, end: Date) {
        const user = this.ls.getUser();
        return this.request.post('/mealCalendar/byDateRange', {user,start,end});
    }
    getMealCalendarOnDay(day: Date) {
        const user = this.ls.getUser();
        return this.request.post('/mealCalendar/onDay', {user, day});
    }
}