import {Injectable} from '@angular/core';
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');

@Injectable()
export class UtilService {
  isBlank(value) {
    if (value && value.trim()) {
      return value.trim();
    }
    return '';
  }
  isMobile() {
    return window.navigator.userAgent.match(/mobile/i);
  }

  userWeightKG(weight: number, metric: string): number {
    if (metric === 'kilograms') {
      return weight;
    } else if (metric === 'pounds') {
      return weight * 0.45359237;
    } else return weight;
  }
  userHeightCM(bigger: number, smaller: number, metric) {
    if (metric === 'imperial') {
      const m = this.convertImperialToMetric(bigger, smaller);
      return this.convertMetricToCm(m.bigger, m.smaller);
    } else if (metric === 'metric') {
      return this.convertMetricToCm(bigger, smaller);
    } else {
      return this.convertMetricToCm(bigger, smaller);
    }
  }
  getUserAge(dateOfBirth: Date): number {
    return moment().diff(dateOfBirth,'year');
  }
  cmToFullMetric(cm: number): {bigger: number, smaller: number} {
    return {bigger: Math.floor(cm / 100), smaller: cm - (Math.floor(cm / 100) * 100)}
  }
  inchesToFullImperial(inches: number): {bigger: number, smaller: number} {
    return {bigger: Math.floor(inches / 12), smaller: inches - (Math.floor(inches / 12) * 12)}
  }
  convertImperialToMetric(feet: number, inches:number): {bigger: number, smaller: number} {
    return this.cmToFullMetric(this.convertImperialToInches(feet, inches));
  }
  convertMetricToImperial(meters: number, cm:number): {bigger: number, smaller: number} {
    cm += (meters * 12);
    return this.inchesToFullImperial(this.convertMetricToCm(meters, cm));
  }
  convertImperialToInches(feet: number, inches: number): number {
    inches += (feet * 12);
    return inches;
  }
  convertMetricToCm(meters: number, cm: number): number {
    cm += (meters * 12);
    return cm;
  }
}
