import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';
import {User} from '../models/user.model';

@Injectable()
export class RequestService {
  authorization: string;
  api = environment.appAPI;

  constructor(private http: HttpClient) {}

  signup(user: User) {
    return this.http.post(`${this.api}/authenticate/signup`, user);
  }
  login(username: string, password: string) {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Accept', 'application/json');
    headers = headers.append('Content-Type', 'application/json');
    return new Observable(sub => {
      this.http.post(`${this.api}/authenticate/login`, {username: username, password: password}, { headers }).subscribe(res => {
            this.updateToken(res['token']);
            sub.next(res);
            sub.complete();
          },
          error => {
            sub.error(error);
            sub.complete();
          }
        );
    });
  }
  updateToken(token: string) {
    this.authorization = token;
  }
  updateUser(user: User) {
    return this.post('user/update', user);
  }

  get(path, query?: any) {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Authorization', `Bearer ${this.authorization}`);
    headers = headers.append("Access-Control-Allow-Origin", environment.appAPI);
    let queryParams = '';
    if (query && Object.keys(query).length > 0) {
      queryParams = '?';
      const params = [];
      Object.keys(query).forEach(param => {
        params.push(`${param}=${encodeURIComponent(query[param])}`);
      });
      queryParams += params.join('&');
    }
    return this.http.get(`${this.api}/${path}${queryParams}`, { headers });
  }

  post(path, data) {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Accept', 'application/json');
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', `Bearer ${this.authorization}`);
    headers = headers.append("Access-Control-Allow-Origin", environment.appAPI);
    return this.http.post(`${this.api}/${path}`, data, { headers });
  }
}
