import {Injectable} from '@angular/core';
import {Food, Meal, MealCalendar, MealFood} from '../models/meal.module';
import {LoginService} from './login.service';
import {HelperService} from './helper.service';
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');
import {Observable} from 'rxjs/internal/Observable';
import {User} from '../models/user.model';
import { RequestService } from './request.service';
import { EdamamMeasure, MealType } from '../models/edamam.model';

@Injectable()
export class MealService {
  constructor(private loginService: LoginService, private helper: HelperService, private request: RequestService) {}

  getMealCalendarByDateRange(user: User, start: Date, end: Date): Observable<MealCalendar[]> {

    return this.request.post('meal/calendar', {
      user, start, end
    }) as Observable<MealCalendar[]>;
  }
  createMeal(user: User, foodId: string, type: MealType, label: string, eatenDate: Date, eatenAmount: number, measure: EdamamMeasure, qualifier?: EdamamMeasure): Observable<Meal> {
    return this.request.post('meal/create', {user,foodId, label, eatenDate,eatenAmount,measure: measure.uri,qualifier, type}) as Observable<Meal>;
  }
  modifyMeal(user: User, meal: Meal) {
    return this.request.post(`meal/update`, {user, meal}) as Observable<any>;
  }
  removeMeal(meal: Meal): Observable<any> {
    return this.request.get(`meal/delete/${meal.id}`) as Observable<any>;
  }
  getMealDisplay(meal: Meal): string {
      if (meal && meal.label) {
        if (meal.eatenAmount >= 0 && meal.measure && meal.measure.label) {
          return `${meal.eatenAmount} ${meal.measure.label} ${meal.label}`;
        } else return meal.label;
      } else return '';
  }
  getFoodContentsDisplay(foods: [MealFood]): string {
    const displayItems = [];
    foods.forEach(food => {
      displayItems.push(`${food.food.serving.amount} ${food.food.serving.metric} ${food.food.name}`);
    });
    if (displayItems.length > 0) {
      return displayItems.join(', ');
    } else {
      return '';
    }
  }
  updateMealType(meal: Meal, mealType: MealType) {
    return this.request.post(`meal/update/mealType`,{meal,mealType}) as Observable<Meal>;
  }
  duplicateMeal(user: User, meal: Meal, date: Date): Observable<Meal> {
    return this.createMeal(user, meal.foodId, meal.type, meal.label, date, meal.eatenAmount, meal.measure, meal.qualifier) as Observable<Meal>;
  }
  isBreakFast(meal: Meal): boolean {
    return meal ? meal.type === MealType.BREAKFAST : false;
    // return meal && meal.type && meal.type.toUpperCase() === 'BREAKFAST';
  }
  isLunch(meal: Meal): boolean {
    return meal ? meal.type === MealType.LUNCH : false;
    // return meal && meal.type && meal.type.toUpperCase() === 'LUNCH';
  }
  isDinner(meal: Meal): boolean {
    return meal ? meal.type === MealType.DINNER : false;
    // return meal && meal.type && meal.type.toUpperCase() === 'DINNER';
  }
  isSnack(meal: Meal): boolean {
    return meal ? meal.type === MealType.SNACK : false;
    // return (meal && meal.type && meal.type.toUpperCase() === 'SNACK') || (!this.isBreakFast(meal) || !this.isLunch(meal) || !this.isDinner(meal));
  }
  isTeatime(meal: Meal): boolean {
    return meal ? meal.type === MealType.TEATIME : false;
    // return (meal && meal.type && meal.type.toUpperCase() === 'SNACK') || (!this.isBreakFast(meal) || !this.isLunch(meal) || !this.isDinner(meal));
  }
  // private getTestMealCalendar(start: Date, end: Date): MealCalendar[] {
  //   const cal: MealCalendar[] = [];
  //   for (let day = start; moment(day).isSameOrBefore(moment(end)); day = moment(day).add(1, 'days').toDate()) {
  //     const mc = new MealCalendar();
  //     mc.user = 'swysoc1@gmail,com';
  //     mc.date = this.helper.startOfDay(day);
  //     const meal1 = new Meal();
  //     meal1.name = 'Cheese Pizza';
  //     meal1.type = 'Breakfast';

  //     const cheese = new Food();
  //     cheese.serving.amount = 2;
  //     cheese.serving.metric = '1 Cup';
  //     cheese.protein = 8;
  //     cheese.carb = 3;
  //     cheese.fat = 8;
  //     cheese.name = 'Mozzarella';
  //     const cheeseMeal = new MealFood();
  //     cheeseMeal.food = cheese;
  //     cheeseMeal.servingAmount = 1;

  //     const dough = new Food();
  //     dough.serving.amount = 3;
  //     dough.serving.metric = '1 Cup';
  //     dough.protein = 10;
  //     dough.carb = 22;
  //     dough.fat = 6;
  //     dough.name = 'Pizza Dough';
  //     const doughMeal = new MealFood();
  //     doughMeal.food = cheese;
  //     doughMeal.servingAmount = 2;

  //     const sauce = new Food();
  //     sauce.serving.amount = 1;
  //     sauce.serving.metric = '6 Oz';
  //     sauce.protein = 0;
  //     sauce.carb = 8;
  //     sauce.fat = 2;
  //     sauce.name = 'Marinara Sauce';
  //     const sauceMeal = new MealFood();
  //     sauceMeal.food = cheese;
  //     sauceMeal.servingAmount = 1;

  //     meal1.foods = [];
  //     meal1.foods.push(cheeseMeal, sauceMeal, doughMeal);
  //     mc.meals = [];
  //     mc.meals.push(meal1);
  //     cal.push(mc);
  //   }
  //   return cal;
  // }
  // isFavoriteFood(food: Food) {
  //   return this.loginService.getUserSession().user.favFoods.some(favFoodId => favFoodId === food.id);
  // }
  // isFavoriteMeal(meal: Meal) {
  //   return this.loginService.getUserSession().user.favMeals.some(favMealId => favMealId === meal.id);
  // }
}
