import { EventEmitter, Injectable } from '@angular/core';
import { Permission, Role, User, UserSession } from '../models/user.model';
import { Observable } from 'rxjs';
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');
import { RequestService } from './request.service';

@Injectable()
export class LoginService {
  private _userSession: UserSession = new UserSession();
  getUserSessionUpdates: EventEmitter<UserSession> = new EventEmitter<UserSession>();
  rememberMe = false;
  getUserSession(): UserSession {
    return this._userSession;
  }
  private setUserSession(value: UserSession) {
    this._userSession = value;
    this.cacheSession(value);
    this.getUserSessionUpdates.emit(this._userSession);
  }

  constructor(private request: RequestService) {
    const cachedSession: UserSession = JSON.parse(
      localStorage.getItem('CFBlocks')
    );
    if (
      cachedSession &&
      cachedSession.user.username &&
      cachedSession.lastLogin &&
      moment(cachedSession.lastLogin).isSameOrAfter(
        moment().subtract(1, 'days')
      )
    ) {
      this.rememberMe = true;
      this.setUserSession(cachedSession);
    } else {
      localStorage.removeItem('CFBlocks');
    }
  }
  login(
    username: string,
    password: string,
    rememberMe: boolean
  ): Observable<UserSession> {
    this.rememberMe = rememberMe;
    return new Observable(subscriber => {
      localStorage.removeItem('CFBlocks');
      this.request.login(username, password).subscribe(
        res => {
          const userSession = new UserSession();
          userSession.created = moment().toDate();
          userSession.lastLogin = userSession.created;
          userSession.authenticated = true;
          userSession.user = res as User;
          this.setUserSession(userSession);
          subscriber.next(userSession);
          subscriber.complete();
        },
        error => {
          subscriber.error(error);
          subscriber.complete();
        }
      );
    });
  }
  logout(): Observable<UserSession> {
    return new Observable(subscriber => {
      try {
        localStorage.removeItem('CFBlocks');
        this.setUserSession(null);
        this.request.updateToken(null);
        subscriber.next(this.getUserSession());
        subscriber.complete();
      } catch (error) {
        subscriber.error(error);
        subscriber.complete();
      }
    });
  }
  signup(user: User) {
    return new Observable(subscriber => {
      this.request.signup(user).subscribe(
        signupRes => {
          this.login(user.username, user.password, true).subscribe(
            loginRes => {
              subscriber.next(loginRes);
              subscriber.complete();
            },
            error => {
              subscriber.error(error);
              subscriber.complete();
            }
          );
        },
        error => {
          subscriber.error(error);
          subscriber.complete();
        }
      );
    });
  }
  cacheSession(userSession: UserSession) {
    if (this.rememberMe || true) {
      localStorage.setItem('CFBlocks', JSON.stringify(userSession));
    }
  }
  updateUser(user: User): Observable<User> {
    return new Observable(subscriber => {
      this.request.updateUser(user).subscribe((newUser: User) => {
        const newUserSession = this.getUserSession();
        newUserSession.user = newUser;
        this.setUserSession(newUserSession);
        subscriber.next(newUser);
        subscriber.complete();
      }, error => {
        subscriber.error(error);
        subscriber.complete();
      });
    });
  }
  getUser(): User {
    const userSession = this.getUserSession();
    return userSession.user;
  }
  isAdmin() {
    return false;
  }
  hasAdmin() {
    return false;
  }
  toggleAdmin() {
    // TODO
  }
  getFormatedName(): string {
    const user = this.getUser();
    return `${user.firstName} ${user.lastName}`;
  }
}
