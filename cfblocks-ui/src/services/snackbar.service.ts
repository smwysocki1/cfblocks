import { Injectable } from "@angular/core";
import { MatSnackBar, MatSnackBarRef } from "@angular/material/snack-bar";
import { environment } from "../environments/environment";

@Injectable()
export class SnackBarService {
  constructor(private snackBar: MatSnackBar) {}
  open(message: string, stayTillDismissed?: boolean): MatSnackBarRef<any> {
    return this.snackBar.open(message, 'Dismiss', {
      panelClass: 'custom-snack-bar',
      duration: stayTillDismissed ? undefined : environment.sbDefaultDuration * 1000,
      politeness:'polite'
    });
  }
  error(error: any, stayTillDismissed?: boolean) {
    let message = '';
    if (typeof error == 'string') {
      message = error;
    } else  {
      if (error && error.error && typeof error.error !== 'string') {
        error = error.error;
      }
      message += error && error.code ? error.code + ': ' : error.status ? error.status + ': ' : '';
      message += error ? error.message ? error.message : error.name ? error.name: 'Unknown Error' : 'Unknown Error'; 
    }
    return this.snackBar.open(message, 'Dismiss', {
      panelClass: 'custom-snack-bar.error',
      duration: stayTillDismissed ? undefined : environment.sbDefaultDuration * 1000,
      politeness:'polite'
    });
  }
  close(snackBarRef: MatSnackBarRef<any>): void {
    snackBarRef.dismiss();
  }

}