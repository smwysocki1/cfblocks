import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { CusineType, DietLabel, DishType, EdamamCaution, EdamamFood, EdamamFoodHint, EdamamFoodRes, EdamamNutrientsRes, HealthLabel, MealType } from "../models/edamam.model";
import { RequestService } from "./request.service";
import  healthLabels from  '../app/data/healthLabel.json';
import  dietLabels from  '../app/data/dietLabel.json';

@Injectable()
export class EdamamService {
    constructor(private request: RequestService){}

    search(q: string, filters: any): Observable<EdamamFoodRes>{
      return this.request.post('edamam/food/search', {q, filters}) as Observable<EdamamFoodRes>
    }
    getFood(foodId: string, label?: string): Observable<EdamamFoodHint> {
      return this.request.get(`edamam/food/byFoodId`, {foodId, label}) as Observable<EdamamFoodHint>;
    }
    getNurtients(foodId: string, quantity: number, measure: string, qualifiers?: string): Observable<EdamamNutrientsRes> {
      const params: any = {foodId, quantity, measure, cacheFirst: true};
      if (qualifiers) {
        params.qualifiers = qualifiers;
      }
      return this.request.get('edamam/food/nutrients', params) as Observable<EdamamNutrientsRes>;
    }
    nextPage(uri: string): Observable<EdamamFoodRes> {
      return this.request.post('edamam/food/next_page', {uri}) as Observable<EdamamFoodRes>;
    }
    getDietLabels(): {type: string, 'web-label': string, 'api-label': string, definition: string, label?: string}[] {
      return dietLabels;
    }
    getHealthLabels(): {type: string, 'web-label': string, 'api-label': string, definition: string, label?: string}[] {
      return healthLabels;
    }
    getCautionLabels() {
      return this.request.get('edamam/labels/caution') as Observable<EdamamCaution[]>;
    }
    getMealTypes(): (string | MealType)[] {
      return Object.values(MealType);
    }
    getDishTypes(): (string | DishType)[] {
      return Object.keys(DishType);
    }
    getCusineType(): (string | CusineType)[] {
      return Object.keys(CusineType);
    }
    
}