import {Injectable} from '@angular/core';
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');

@Injectable()
export class HelperService {
  startOfDay(date?: Date): Date {
    if (!date) {
      date = moment().toDate();
    }
    return moment(date).startOf('day').toDate();
  }
  endOfDay(date?: Date): Date {
    if (!date) {
      date = moment().toDate();
    }
    return moment(date).endOf('day').toDate();
  }
}
