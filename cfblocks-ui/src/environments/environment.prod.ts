export const environment = {
  production: true,
  hmr: false,
  appAPI: 'https://api.essentialnutritionplanning.com',
  defaultImg: '/assets/imgs/favicon.png',
  themes: ['kitchen-theme'],
  facebook: '',
  twitter: '',
  youtube: '',
  instagram: '',
  sbDefaultDuration: 3
};
