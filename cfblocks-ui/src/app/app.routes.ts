import { Routes } from '@angular/router';
import {HomeComponent} from './containers/home/home.component';
import {UserAccountComponent} from './component/userAccount/userAccount.component';
import {BlockCalculatorComponent} from './containers/block-calculator/block-calculator.component';
// import {MealBuilderComponent} from './component/mealBuilder/mealBuilder.component';
// import {AdminComponent} from './containers/admin/admin.component';
import {SigninComponent} from './containers/signin/signin.component';
import {SignupComponent} from './containers/signup/signup.component';
import {NotFoundComponent} from './containers/not-found/not-found.component';
import {PlannerComponent} from './containers/planner/planner.component';
import {MealCalendarComponent} from './containers/meal-calendar/meal-calendar.component';
// import {MealEditorComponent} from './component/mealBuilder/mealEditor/mealEditor.component';
// import {FoodCreatorComponent} from './component/mealBuilder/food-creator/food-creator.component';
import { FoodFinderComponent } from './component/foodFinder/foodFinder.component';
import { MainMenuComponent } from './containers/main-menu/main-menu.component';
import { GroceryListComponent } from './containers/grocery-list/grocery-list.component';
import { ResourcesComponent } from './containers/resources/resources.component';
import { DefaultMealsComponent } from './component/defaultMeals/defaultMeals.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  // { path: 'admin', component: AdminComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'planner', component: PlannerComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'main-menu', component: MainMenuComponent },
  { path: 'account', component: UserAccountComponent},
  // { path: 'block-calculator', component: BlockCalculatorComponent },
  { path: 'meal-calendar', component: MealCalendarComponent },
  { path: 'resources', component: ResourcesComponent},
  { path: 'resources/:page', component: ResourcesComponent},
  { path: 'default-meals', component: DefaultMealsComponent},
  // { path: 'meal-builder/:date', component: MealBuilderComponent },
  // { path: 'meal-builder/:date/meal/:meal', component: MealEditorComponent },
  // { path: 'meal-builder/:date/meal/:meal/food/:food', component: FoodCreatorComponent },
  { path: 'food-finder', component: FoodFinderComponent },
  { path: 'grocery-list', component: GroceryListComponent },
  { path: '**', redirectTo: '/not-found' },
];
