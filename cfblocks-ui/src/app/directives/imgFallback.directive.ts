import {Directive, Input, HostBinding} from '@angular/core'
import { environment } from '../../environments/environment';
@Directive({
    selector: 'img',
    host: {
      '(onerror)':'updateUrl()',
      '[src]':'src'
     }
  })
  
 export class ImageFallbackDirective {
    @Input() src:string;
    // @HostBinding('class') className
  
    updateUrl() {
      this.src = window.location.origin + environment.defaultImg;
    }
    // load(){
    //   this.className = 'image-loaded';
    // }
  }