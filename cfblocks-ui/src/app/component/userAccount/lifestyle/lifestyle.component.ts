import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { DietLabel, EdamamCaution, HealthLabel } from '../../../../models/edamam.model';
import { EdamamService } from '../../../../services/edamam.service';
import {BodyWeight, mergeUser, User, UserSession} from '../../../../models/user.model';
import {LoginService} from '../../../../services/login.service';
import {ValidationService} from '../../../../services/validation.service';
import { InvokeFunctionExpr } from '@angular/compiler';

@Component({
  selector: 'user-lifestyle',
  templateUrl: 'lifestyle.html',
  styles: [`
  .form-container {
    display: flex;
    flex-direction: column;
  }
  .form-section {
    display: flex;
    flex-direction: row;
  }
  .form-container > *,
  .form-section > * {
    width: 100%;
  }
  `]
})
export class UserLifeStyleComponent implements OnInit, OnChanges {
  @Input() user: User;
  @Input() form: FormGroup;
  @Input() updateActive: boolean;
  @Input() fieldSetName: string;
  @Input() enableCancel: boolean;
  @Input() cautionLabelResults: EdamamCaution[];
  @Input() dietLabelResults: {type: string, 'web-label': string, 'api-label': string, definition: string}[];
  @Input() healthLabelResults: {type: string, 'web-label': string, 'api-label': string, definition: string}[];
  @Output() toggleUpdateActive = new EventEmitter<string>();
  @Output() updateUser = new EventEmitter<User>();
  constructor(private ls: LoginService, private fb: FormBuilder, private vs: ValidationService, private es: EdamamService) {}
  ngOnInit() {
    this.loadUserToForm();
    this.nutritionPlan.valueChanges.subscribe(event => {
      if (event == 'vegan-zone') {
        this.healthLabels.get('Vegan').patchValue(true);
        this.healthLabels.get('Vegetarian').patchValue(true);
      }
    })
  }
  get nutritionPlan() {return this.form.get('nutritionPlan');};
  get healthLabels() {return this.form.get('healthLabels');};
  get dietLabels() {return this.form.get('dietLabels');};
  get cautionLabels() {return this.form.get('cautionLabels');};
  loadUserToForm() {
    this.form.patchValue({
      nutritionPlan: this.user.nutritionPlan
    });
    this.patchLabels(this.healthLabelResults, this.healthLabels as FormGroup, this.user.preferedHealthLabels);
    this.patchLabels(this.dietLabelResults, this.dietLabels as FormGroup, this.user.preferedDietLabels);
    this.patchCautionLabels(this.cautionLabelResults.map(c => c.toString()), this.cautionLabels as FormGroup, this.user.preferedCautionLabels);
  }
  onSubmit() {
    if (this.form.valid) {
      const data = this.form.value;
      const user: User = new User();
      user.nutritionPlan = data.nutritionPlan;
      user.preferedCautionLabels = this.parseResult(this.cautionLabels.value);
      user.preferedHealthLabels = this.parseResult(this.healthLabels.value);
      user.preferedDietLabels = this.parseResult(this.dietLabels.value);
      this.updateUser.emit(user);
    } else {
      this.vs.validateAllFormFields(this.form);
    }
  }
  parseResult(value: any[]) {
    const result = [];
    if (value && Object.keys(value).length > 0) {
      Object.keys(value).forEach(param => {
        if (value[param]) {
          result.push(param);
        }
      })
    }
    return result;
  }
  reset() {
    this.loadUserToForm();
  }
  patchLabels(labelResults: {type: string, 'web-label': string, 'api-label': string, definition: string}[], label: FormGroup, preferedLabels: string[]) {
    labelResults.forEach(labelResult => {
      const patch = {};
      // patch[this.getLabel(labelResult)] = this.user.preferedDietLabels ? this.user.preferedDietLabels.some(dl => dl === this.getLabel(labelResult)) : false;
      const formControl = label.get(this.getLabel(labelResult));
      if (formControl) {
        formControl.patchValue(preferedLabels ? preferedLabels.some(dl => dl === this.getLabel(labelResult)) : false);
      }
    });
  }
  patchCautionLabels(labelResults: string[], label: FormGroup, preferedLabels: string[]) {
    labelResults.forEach(labelResult => {
      const patch = {};
      const formControl = label.get(labelResult);
      if (formControl) {
        formControl.patchValue(preferedLabels ? preferedLabels.some(dl => dl === labelResult) : false);
      }
    });
  }
  getLabel(item: {type: string, 'web-label': string, 'api-label': string, definition: string, label?: string}) {
    if (item) {
      return item['web-label'];
    } else return '';
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes){
      if (changes.form && changes.form.currentValue) {
        this.loadUserToForm();
      }
      if (changes.user && changes.user.currentValue) {
        this.loadUserToForm();
      }
      if (changes.dietLabelResults && changes.dietLabelResults.currentValue) {
        this.patchLabels(this.dietLabelResults, this.dietLabels as FormGroup, this.user.preferedDietLabels);
      }
      if (changes.healthLabelResults && changes.healthLabelResults.currentValue) {
        this.patchLabels(this.healthLabelResults, this.healthLabels as FormGroup, this.user.preferedHealthLabels);
      }
      if (changes.cautionLabelResults && changes.cautionLabelResults.currentValue) {
        this.patchCautionLabels(this.cautionLabelResults.map(c => c.toString()), this.cautionLabels as FormGroup, this.user.preferedCautionLabels);
      }
    }
  }
}
