import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../../models/user.model';
import {LoginService} from '../../../../services/login.service';
import {ValidationService} from '../../../../services/validation.service';
import { BlockCalculatorService } from '../../../../services/block-calculator.service';
import { UtilService } from '../../../../services/util.service';

@Component({
  selector: 'user-body-info',
  templateUrl: 'userBodyInfo.html',
  styles: [`
  .form-container {
    display: flex;
    flex-direction: column;
  }
  .form-section {
    display: flex;
    flex-direction: row;
  }
  .form-container > *,
  .form-section > * {
    width: 100%;
  }
  `]
})
export class UserBodyInfoComponent implements OnInit, OnChanges {
  @Input() user: User;
  @Input() form: FormGroup;
  @Input() updateActive: boolean;
  @Input() fieldSetName: string;
  @Input() enableCancel: boolean;
  @Output() toggleUpdateActive = new EventEmitter<string>();
  @Output() updateUser = new EventEmitter<User>();
  metric: string;
// https://loiane.com/2017/08/angular-reactive-forms-trigger-validation-on-submit/
  constructor(private ls: LoginService, private fb: FormBuilder, private vs: ValidationService, private bsc: BlockCalculatorService, private utils: UtilService) {}
  ngOnInit() {
    this.loadUserToForm();
  }
  loadUserToForm() {
    let height = {bigger:0, smaller:0};
    if (this.user && this.user.height > 0) {
      if (this.user.heightMetric === "imperial") {
        this.metric = 'imperial';
        height = this.utils.cmToFullMetric(this.user.height);
      } else if (this.user.heightMetric === "metric"){
        this.metric = 'metric';
        height = this.utils.inchesToFullImperial(this.user.height);
      }
    }
    this.form.patchValue({
      bodyWeight: this.user.bodyWeight,
      bodyWeightMetric: this.user.bodyWeightMetric,
      heightBigger: height.bigger,
      heightSmaller: height.smaller,
      heightMetric: this.user.heightMetric,
      activityLevel: this.user.activityLevel
    })
  }
  get bodyWeight() { return this.form.get('bodyWeight');}
  get bodyWeightMetric() { return this.form.get('bodyWeightMetric');}
  get heightBigger() { return this.form.get('heightBigger');}
  get heightSmaller() { return this.form.get('heightSmaller');}
  get heightMetric() { return this.form.get('heightMetric');}
  get activityLevel() { return this.form.get('activityLevel');};

  onSubmit() {
    if (this.form.valid) {
      const data = this.form.value;
      const user: User = new User();
      user.bodyWeight = data.bodyWeight;
      user.bodyWeightMetric = data.bodyWeightMetric;
      if (data.heightMetric === 'imperial') {
        user.height = (data.heightBigger * 12) + data.heightSmaller;
      } else {
        user.height = (data.heightBigger * 100) + data.heightSmaller;
      }
      user.heightMetric = data.heightMetric;
      user.activityLevel = data.activityLevel;
      this.updateUser.emit(user);
    } else {
      this.vs.validateAllFormFields(this.form);
    }
  }
  toggleUpdate() {
    this.toggleUpdateActive.emit(this.fieldSetName);
  }
  reset() {
    this.loadUserToForm();
    this.toggleUpdateActive.emit(null);
  }
  get calculateBMI(): number {
    if (this.form && this.bodyWeight.valid && this.bodyWeightMetric.valid && this.heightBigger.valid && this.heightSmaller.valid && this.heightMetric.valid) {
      return this.bsc.calculateBMI(
        this.user.sex, 
        this.utils.userWeightKG(this.bodyWeight.value, this.bodyWeightMetric.value), 
        this.utils.userHeightCM(this.heightBigger.value, this.heightSmaller.value, this.heightMetric.value),
        this.utils.getUserAge(this.user.dob)
      );
    } else return 0;
  }
  get calculateBMR(): number {
    if (this.form && this.activityLevel.valid) {
      return this.bsc.calculateBMR(
        this.user.sex, 
        this.utils.userWeightKG(this.bodyWeight.value, this.bodyWeightMetric.value), 
        this.utils.userHeightCM(this.heightBigger.value, this.heightSmaller.value, this.heightMetric.value),
        this.utils.getUserAge(this.user.dob),
        this.activityLevel.value);
    } else return 0;
  }
  displayHeightMetric(size:string) {
    if (this.heightMetric && this.heightMetric.valid) {
      if (this.heightMetric.value === 'imperial') {
        switch(size) {
          case 'bigger': return 'Feet'; break;
          case 'smaller': return 'Inches'; break;
        }
      } else if (this.heightMetric.value === 'metric') {
        switch(size) {
          case 'bigger': return 'Meters'; break;
          case 'smaller': return 'Centimeters'; break;
        }
      }
    }
    return '';
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.user && changes.user.currentValue) {
      this.loadUserToForm();
    }
  }
}
