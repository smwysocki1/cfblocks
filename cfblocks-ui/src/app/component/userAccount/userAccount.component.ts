import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/user.model';
import {UtilService} from '../../../services/util.service';
import {LoginService} from '../../../services/login.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { EdamamCaution } from '../../../models/edamam.model';
import { EdamamService } from '../../../services/edamam.service';
import {Router} from '@angular/router';
import { SnackBarService } from '../../../services/snackbar.service';

@Component({
  selector: 'user-account',
  templateUrl: 'userAccount.html'
})
export class UserAccountComponent implements OnInit {
  userAccount: User;
  fieldSet;

  isLinear = false;

  cautionLabels: EdamamCaution[] = [];
  dietLabels: {type: string, 'web-label': string, 'api-label': string, definition: string}[] = [];
  healthLabels: {type: string, 'web-label': string, 'api-label': string, definition: string}[] = [];

  constructor(private util: UtilService, private ls: LoginService, private fb: FormBuilder, private es: EdamamService, private sb: SnackBarService) { }
  userForm: FormGroup;
  get generalInformation() {return this.userForm ? this.userForm.get('generalInformation') : undefined};
  get bodyMassIndex() { return this.userForm ? this.userForm.get('bodyMassIndex') : undefined;};
  get lifestyle() { return this.userForm ? this.userForm.get('lifestyle') : undefined;};

  buildLabelFormArray(labels: {type: string, 'web-label': string, 'api-label': string, definition: string}[]): FormArray {
    const formArray: FormArray = this.fb.array([]);
    if (labels && labels.length > 0) {
      labels.forEach(label => {
        formArray.push(this.fb.control({label, isChecked: false}));
      });
    }
    return formArray;
  }
  buildLabelsFormGroup(labels: {type: string, 'web-label': string, 'api-label': string, definition: string}[]): FormGroup {
    const fg: FormGroup = this.fb.group({});
    labels.forEach(label => {
      fg.addControl(label['web-label'], new FormControl([false]));
    });
    return fg;
  }
  buildCautionLabelsFormGroup(labels: EdamamCaution[]): FormGroup {
    const fg: FormGroup = this.fb.group({});
    labels.forEach(label => {
      fg.addControl(label.toString(), new FormControl([false]));
    });
    return fg;
  }
  async ngOnInit() {
    this.userAccount = this.ls.getUser() as User;
    this.ls.getUserSessionUpdates.subscribe(update => {
      this.userAccount = update.user;
    });
    await this.getLabels();
    this.userForm = this.fb.group({
      generalInformation: this.fb.group({
        email: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        dob: [Date.now(), Validators.required],
        sex: ['', Validators.required]
      }),
      bodyMassIndex: this.fb.group({
        bodyWeight: [0, Validators.required],
        bodyWeightMetric: ['pounds', Validators.required],
        heightBigger: [0, Validators.required],
        heightSmaller: [0, Validators.required],
        heightMetric: ['imperial', Validators.required],
        activityLevel: ['', Validators.required]
      }),
      lifestyle: this.fb.group({
        nutritionPlan: ['', Validators.required],
        healthLabels: this.buildLabelsFormGroup(this.healthLabels),
        dietLabels: this.buildLabelsFormGroup(this.dietLabels),
        cautionLabels: this.buildCautionLabelsFormGroup(this.cautionLabels)
      })
    });
  }
  async updateUser(user: User) {
    try {
      const newUser = {...this.userAccount, ...user};
      await this.ls.updateUser(newUser).toPromise();
      this.sb.open('Saved User Info');
    } catch(error) {
      console.error(error);
      this.sb.error(error);
    }
  }
  completed(formControl: FormControl) {
    if (formControl) {
      return formControl.valid && !formControl.dirty;
    } else return false;
  }

  async getLabels() {
    this.cautionLabels = await this.es.getCautionLabels().toPromise();
    this.dietLabels = this.es.getDietLabels();
    this.healthLabels = this.es.getHealthLabels();
  }
}
