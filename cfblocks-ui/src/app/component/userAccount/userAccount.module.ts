import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserAccountComponent} from './userAccount.component';
import {UtilService} from '../../../services/util.service';
import {UserGeneralInfoComponent} from './generalInfo/generalInfo.component';
import {UserBodyInfoComponent} from './userBody/userBodyInfo.component';
import {UserLifeStyleComponent} from './lifestyle/lifestyle.component';
// import {UserBlockTemplateComponent} from './blockTemplate.component.ts/blockTemplate.component';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {MatStepperModule} from "@angular/material/stepper";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatCardModule} from "@angular/material/card";
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
// import {AppRoutingModule} from '../../app.routing-module';
import {RouterModule} from '@angular/router';
@NgModule({
  declarations: [
    UserAccountComponent,
    UserGeneralInfoComponent,
    UserBodyInfoComponent,
    UserLifeStyleComponent,
    // UserBlockTemplateComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule,
    // AppRoutingModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    MatStepperModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
  ],
  exports: [
    UserAccountComponent,
    UserGeneralInfoComponent,
    UserBodyInfoComponent,
    // UserLifeStyleComponent,
    // UserBlockTemplateComponent
  ],
  providers: [UtilService]
})
export class UserAccountModule { }
