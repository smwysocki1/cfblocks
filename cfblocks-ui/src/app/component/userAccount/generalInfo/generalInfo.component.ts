import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../../models/user.model';
import {LoginService} from '../../../../services/login.service';
import {ValidationService} from '../../../../services/validation.service';
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');

@Component({
  selector: 'user-general-info',
  templateUrl: 'generalInfo.html',
  styles: [`
  .form-container {
    display: flex;
    flex-direction: column;
  }
  
  .form-container > * {
    width: 100%;
  }
  `]
})
export class UserGeneralInfoComponent implements OnInit, OnChanges {
  @Input() user: User;
  @Input() form: FormGroup;
  @Input() updateActive: boolean;
  @Input() fieldSetName: string;
  @Input() enableCancel: boolean;
  @Output() toggleUpdateActive = new EventEmitter<string>();
  @Output() updateUser = new EventEmitter<User>();
  constructor(private ls: LoginService, private fb: FormBuilder, private vs: ValidationService) {}
  ngOnInit() {
    this.loadUserToForm();
  }
  loadUserToForm() {
    this.form.patchValue({
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      email: this.user.email,
      dob: this.user.dob,
      sex: this.user.sex
    });
  }
  get firstName() {return this.form.get('firstName')};
  get lastName() {return this.form.get('lastName')};
  get email() {return this.form.get('email')};
  get dob() {return this.form.get('dob')};
  get sex() {return this.form.get('sex')};
  ngOnChange(changes: SimpleChanges) {
    if (changes && changes.user && changes.user.currentValue) {
      this.loadUserToForm();
    }
  }
  onSubmit() {
    if (this.form.valid) {
      const user: User = this.form.value;
      this.updateUser.emit(user);
    } else {
      this.vs.validateAllFormFields(this.form);
    }
  }
  clearControl(control: string) {
    this.form.get(control).patchValue(null);
  }
  reset() {
    this.loadUserToForm();
  }
  formatDate(date: Date) {
    if (date) {
      return moment(date).format('MMMM DD YYYY');
    } else {
      return null;
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.user && changes.user.currentValue !== changes.user.previousValue) {
      this.loadUserToForm();
    }
    if (changes.form && changes.form.currentValue) {
      this.loadUserToForm();
    }
  }
}
