import {Component, Input, OnInit, Output, EventEmitter} from "@angular/core";
import { EdamamService } from "../../../../services/edamam.service";
import { EdamamFoodRes, EdamamFood, EdamamMeasure, EdamamFoodHint } from "../../../../models/edamam.model";

@Component({
    selector: 'food-result',
    templateUrl: './foodResult.component.html',
    styleUrls: ['./foodResult.component.css']
})
export class FoodResultComponent implements OnInit {
    constructor(private edamamService: EdamamService) {}
    @Input() food: EdamamFoodHint;
    @Input() selectedFood: EdamamFoodHint;
    @Output() getNutrients: EventEmitter<EdamamFoodHint> = new EventEmitter<EdamamFoodHint>();
    @Output() createMeal: EventEmitter<{food: EdamamFoodHint, quantity: number, measure: EdamamMeasure}> = new EventEmitter<{food: EdamamFoodHint, quantity: number, measure: EdamamMeasure}>();
    moreContents = false;
    ngOnInit() {
      
    }
    addToMealPlan() {
        this.createMeal.emit({food: this.food, quantity: 100, measure: {label: 'Gram', uri: 'http://www.edamam.com/ontologies/edamam.owl#Measure_gram'}});
    }
    imageNotFound() {
        this.food.food.image = '/assets/imgs/favicon.png';
    }
    getNutrition() {
        this.getNutrients.emit(this.food);
    }
    get isSelected() {
        return (this.selectedFood && this.selectedFood.food && this.food && this.food.food && this.food.food.foodId === this.selectedFood.food.foodId && this.food.food.label === this.selectedFood.food.label);
    }
    selectFood() {
        if(this.isSelected) {
            this.addToMealPlan();
        } else {
            this.getNutrition();
        }
    }
}