import {Component, OnInit, ViewChild} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import { EdamamService } from "../../../services/edamam.service";
import { EdamamFoodRes, EdamamFood, EdamamNutrientsRes, EdamamMeasure, EdamamFoodHint, EdamamCaution, MealType } from "../../../models/edamam.model";
import { CreateMealModalComponent } from "./createMealModal/createMealModal.component";
import { MatSnackBar, MatSnackBarRef } from "@angular/material/snack-bar";
import { SnackBarService } from "../../../services/snackbar.service";
import { LoginService } from "../../../services/login.service";
import { MatExpansionPanel } from "@angular/material/expansion";

@Component({
    selector: 'food-finder',
    templateUrl: './foodFinder.component.html',
    styleUrls: ['./foodFinder.component.css']
})
export class FoodFinderComponent implements OnInit {
    constructor(private ls: LoginService, private sbs: SnackBarService, private router: Router, private route: ActivatedRoute, private fb: FormBuilder, private edamamService: EdamamService) {}
    @ViewChild(CreateMealModalComponent, {static: false}) createMealModal;
    @ViewChild('searchFiltersExpansionPanel', {static: false}) searchFiltersExpansionPanel : MatExpansionPanel;
    searchFilterOpened: boolean = false;
    snackBar: MatSnackBarRef<any>;
    foodInFocus: EdamamFoodHint;
    nutrientsInFocus: EdamamNutrientsRes;
    nutrientsQuantityInFocus: number;
    nutrientsMeasurementInFocus: EdamamMeasure;
    searchLoading: boolean = false;
    cautionLabelResults: EdamamCaution[] = [];
    dietLabelResults: {type: string, 'web-label': string, 'api-label': string, definition: string, label?: string}[] = [];
    healthLabelResults: {type: string, 'web-label': string, 'api-label': string, definition: string, label?: string}[] = [];
    searchForm: FormGroup;
    searchResult: EdamamFoodRes;
    addToMealPlanner: boolean;

    get search() { return this.searchForm.get('search') as FormControl }
    get healthLabels() { return this.searchForm.get('healthLabels'); };
    get activeHealthLabels() { 
      if (this.healthLabels && this.healthLabels.value) {
        const labels = this.healthLabels.value;
        return Object.keys(labels).reduce((activeLabels: string[], label:string) => {
          if(labels[label]) activeLabels.push(label)
          return activeLabels;
        }, []);
      } else {
        return [];
      }
    };
    get dietLabels() { return this.searchForm.get('dietLabels'); };
    get activeDietLabels() { 
      if (this.dietLabels && this.dietLabels.value) {
        const labels = this.dietLabels.value;
        return Object.keys(labels).reduce((activeLabels: string[], label:string) => {
          if(labels[label]) activeLabels.push(label)
          return activeLabels;
        }, []);
      } else {
        return [];
      }
    };
    get cautionLabels() { return this.searchForm.get('cautionLabels'); };
    async ngOnInit() {
      // this.route.queryParamMap.pipe(
      //   switchMap((params: ParamsMap, index) => {
      //     console.log(params);
      //     // this.selectedId = Number(params.get('id'));
      //     // return this.service.getHeroes();
      //   })
      // );

      await this.getLabels();
      this.searchForm = this.fb.group({
        search: [{value:'',disabled: this.searchLoading}, Validators.required],
        healthLabels: this.buildLabelsFormGroup(this.healthLabelResults),
        dietLabels: this.buildLabelsFormGroup(this.dietLabelResults),
        cautionLabels: this.buildCautionLabelsFormGroup(this.cautionLabelResults)
      });
      this.patchAdvancedSearchLabels();
      this.route.queryParams.subscribe(async params => {
        if (params) {
          if (params.q) {
            this.searchForm.get('search').patchValue(params.q);
          }
          await this.searchFood();
        }
      });
    }
    clearControl(control: string) {
      this.searchForm.get(control).patchValue(null);
    }
    uncheckFilter(fg: FormGroup, filter: string) {
      fg.get(filter).patchValue(false);
    }
    toggleFilter() {
      this.searchFilterOpened = !this.searchFilterOpened;
      if (this.searchFilterOpened) {
        this.searchFiltersExpansionPanel.open();
      } else {
        this.searchFiltersExpansionPanel.close();
      }
    }
    buildLabelsFormGroup(labels: {type: string, 'web-label': string, 'api-label': string, definition: string}[]): FormGroup {
      const fg: FormGroup = this.fb.group({});
      labels.forEach(label => {
        fg.addControl(label['web-label'], new FormControl([false]));
      });
      return fg;
    }
    buildCautionLabelsFormGroup(labels: EdamamCaution[]): FormGroup {
      const fg: FormGroup = this.fb.group({});
      labels.forEach(label => {
        fg.addControl(label.toString(), new FormControl([false]));
      });
      return fg;
    }
    async searchFood() {
      if (this.searchForm.valid && !this.searchLoading) {
        this.searchSnackBar();
        this.searchLoading = true;
        const filters = {health: this.healthLabels.value, diet: this.dietLabels.value};
        this.edamamService.search(this.search.value, filters).subscribe(async (result: EdamamFoodRes) => {
          this.searchResult = result;
          if (this.searchResult && this.searchResult.hints && this.searchResult.hints.length > 0) {
            const firstFood = this.searchResult.hints[0] as EdamamFoodHint;
            await this.getNutrients(firstFood,100,{label: 'Gram', uri: 'http://www.edamam.com/ontologies/edamam.owl#Measure_gram'});
          }
          this.sbs.close(this.snackBar);
          this.snackBar = undefined;
          this.searchLoading = false;
        }, (error) => {
          if (this.snackBar) {
            this.sbs.close(this.snackBar);
            this.snackBar = undefined;
            }
          this.searchLoading = false;
          this.sbs.error(error);
          console.error(error);
        });
      }
    }

    async getNutrients(food: EdamamFoodHint, quantity?: number, measure?: EdamamMeasure) {
      try {
        this.foodInFocus = food;
        const foodId = food.food.foodId;
        this.nutrientsQuantityInFocus = (quantity === undefined ? 100 : quantity);
        this.nutrientsMeasurementInFocus = food.measures.find(m => m.uri === (measure ? measure.uri : 'http://www.edamam.com/ontologies/edamam.owl#Measure_gram'));
        this.nutrientsInFocus = await this.edamamService.getNurtients(foodId, this.nutrientsQuantityInFocus, this.nutrientsMeasurementInFocus.uri).toPromise();
      } catch(error) {
        this.sbs.error(error);
        console.error(error);
      }
    }

    async getMoreFoods() {
      this.searchLoading = true;
      try {
      const nextPage = await this.edamamService.nextPage(this.searchResult._links.next.href).toPromise();
      this.searchResult._links = nextPage._links;
      this.searchResult.hints = this.searchResult.hints.concat(nextPage.hints);
      this.searchLoading = false;
      } catch(error) {
        console.error(error);
        this.sbs.error(error);
        this.searchLoading = false;
      }
    }
    async updateNutritionInfo(event:{quantity: number, measurement: EdamamMeasure}) {
      if (event.measurement && event.measurement.uri !== this.nutrientsMeasurementInFocus.uri && event.quantity === this.nutrientsQuantityInFocus) {
        event.quantity = 1;
      }
      await this.getNutrients(this.foodInFocus, event.quantity, event.measurement);
    }
    async openCreateMealModal(event: any) {
      if (!this.nutrientsInFocus || !this.foodInFocus || this.foodInFocus.food.foodId != event.food.food.foodId) {
        await this.getNutrients(event.food, event.quantity, event.measure);
      }
      this.addToMealPlanner = true;
      // await this.createMealModal.openModal();
    }
    createMeal(event: {food: EdamamFoodHint, type: MealType, quantity: number, measure: EdamamMeasure, qualifier?: EdamamMeasure}) {
      this.createMealModal.createMeal(event.food, event.type, event.quantity, event.measure, event.qualifier);
    }
    stopMealCreation() {
      this.addToMealPlanner = false;
    }
    searchSnackBar() {
      if (this.snackBar) {
        this.sbs.close(this.snackBar);
        this.snackBar = undefined;
      }
      this.snackBar = this.sbs.open("Searching ...", true);
    }
    getLabel(item: {type: string, 'web-label': string, 'api-label': string, definition: string, label?: string}) {
      if (item) {
        return item['web-label'];
      } else return '';
    }
    getChip(webLabel: string, type: string) {
      if (type === 'dietLabel') {
        const found = this.dietLabelResults.find(l => l['web-label'] === webLabel);
        if (found && found.label) return found.label;
      } else if (type === 'healthLabel') {
        const found = this.healthLabelResults.find(l => l['web-label'] === webLabel);
        if (found && found.label) return found.label
      } 
      return webLabel;
    }
    async getLabels() {
      try {
        this.cautionLabelResults = await this.edamamService.getCautionLabels().toPromise();
        this.dietLabelResults = this.edamamService.getDietLabels();
        this.healthLabelResults = this.edamamService.getHealthLabels();
      } catch(error) {
        this.sbs.error(error);
        console.error(error);
      }
    }
    patchAdvancedSearchLabels() {
      this.patchLabels(this.healthLabelResults, this.healthLabels as FormGroup, this.ls.getUser().preferedHealthLabels);
      this.patchLabels(this.dietLabelResults, this.dietLabels as FormGroup, this.ls.getUser().preferedDietLabels);
      this.patchCautionLabels(this.cautionLabelResults.map(c => c.toString()), this.cautionLabels as FormGroup, this.ls.getUser().preferedCautionLabels);
    }
    patchLabels(labelResults: {type: string, 'web-label': string, 'api-label': string, definition: string}[], label: FormGroup, preferedLabels: string[]) {
      labelResults.forEach(labelResult => {
        const patch = {};
        // patch[this.getLabel(labelResult)] = this.user.preferedDietLabels ? this.user.preferedDietLabels.some(dl => dl === this.getLabel(labelResult)) : false;
        const formControl = label.get(this.getLabel(labelResult));
        if (formControl) {
          formControl.patchValue(preferedLabels ? preferedLabels.some(dl => dl === this.getLabel(labelResult)) : false);
        }
      });
    }
    patchCautionLabels(labelResults: string[], label: FormGroup, preferedLabels: string[]) {
      labelResults.forEach(labelResult => {
        const patch = {};
        const formControl = label.get(labelResult);
        if (formControl) {
          formControl.patchValue(preferedLabels ? preferedLabels.some(dl => dl === labelResult) : false);
        }
      });
    }
}