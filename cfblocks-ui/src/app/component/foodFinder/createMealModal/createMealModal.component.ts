import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import { EdamamService } from "../../../../services/edamam.service";
import { EdamamFoodRes, EdamamFood, EdamamNutrientsRes, EdamamMeasure, MealType } from "../../../../models/edamam.model";
import { Measures } from 'edamam-api';
import { CalendarService } from "../../calendar/calendar.service";
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');
declare var $: any;
import { CalendarDay } from "../../calendar/calendar.model";
import { Meal, MealCalendar } from "../../../../models/meal.module";
import { LoginService } from "../../../../services/login.service";
import { MealService } from "../../../../services/meal.service";
import { Subject } from "rxjs";
import { CalendarComponent } from "../../calendar/calendar.component";
import { SnackBarService } from "../../../../services/snackbar.service";

@Component({
    selector: 'create-meal-modal',
    templateUrl: './createMealModal.component.html',
    styleUrls: ['./createMealModal.component.css']
})
export class CreateMealModalComponent implements OnInit {
    constructor(private router: Router, private fb: FormBuilder, private edamamService: EdamamService, private cs:CalendarService,
      private ls: LoginService, private ms: MealService, private sb: SnackBarService) {}
    @Input() food: {food: EdamamFood, measures: EdamamMeasure[]};
    @Input() nutrients: EdamamNutrientsRes;
    @Input() quantity: number;
    @Input() measurement: EdamamMeasure;
    @Input() qualifier: EdamamMeasure;
    @Output() goBack: EventEmitter<boolean> = new EventEmitter<boolean>();
    @ViewChild(CalendarComponent, {static: false}) calendar;
    day: CalendarDay;
    meals: Meal[];
    mealNutrients;
    selectedDate: Date;
    mealCalendarUpdate: Subject<Date[]> = new Subject<Date[]>();
    async ngOnInit() {
      // await this.getMeal();
    }
    async openModal() {
      // await this.getMeal();
      // $('#create-meal-modal').modal('show');
    }
    closeModal() {
      // $('#create-meal-modal').modal('hide');
    }
    back() {
      this.goBack.emit(true);
    }
    // {food: this.food, quantity: this.quantity, measure: this.measurement.uri}
    async createMeal(food: {food: EdamamFood, measures: EdamamMeasure[]}, type: MealType,  quantity: number, measurement: EdamamMeasure, qualifier?: EdamamMeasure) {
      try {
        await this.ms.createMeal(this.ls.getUserSession().user, food.food.foodId, type, food.food.label, this.selectedDate, quantity, measurement, qualifier).toPromise();
        this.mealCalendarUpdate.next([this.selectedDate]);
        this.sb.open(`${quantity} ${measurement.label} ${food.food.label} has been added to ${moment(this.selectedDate).format('MMMM Do, YYYY')}`);
      } catch(err) {
        this.sb.error(err);
        console.error(err);
      }
    }
    async createBreakfastMeal(food: {food: EdamamFood, measures: EdamamMeasure[]}, quantity: number, measurement: EdamamMeasure, qualifier?: EdamamMeasure) {
      await this.createMeal(food, MealType.BREAKFAST, quantity, measurement, qualifier);
    }
    async createLunchMeal(food: {food: EdamamFood, measures: EdamamMeasure[]}, quantity: number, measurement: EdamamMeasure, qualifier?: EdamamMeasure) {
      await this.createMeal(food, MealType.LUNCH, quantity, measurement, qualifier);
    }
    async createDinnerMeal(food: {food: EdamamFood, measures: EdamamMeasure[]}, quantity: number, measurement: EdamamMeasure, qualifier?: EdamamMeasure) {
      await this.createMeal(food, MealType.DINNER, quantity, measurement, qualifier);
    }
    async createSnackMeal(food: {food: EdamamFood, measures: EdamamMeasure[]}, quantity: number, measurement: EdamamMeasure, qualifier?: EdamamMeasure) {
      await this.createMeal(food, MealType.SNACK, quantity, measurement, qualifier);
    }
    async createTeatimeMeal(food: {food: EdamamFood, measures: EdamamMeasure[]}, quantity: number, measurement: EdamamMeasure, qualifier?: EdamamMeasure) {
      await this.createMeal(food, MealType.TEATIME, quantity, measurement, qualifier);
    }
    getWindowHeight() {
      return window.innerHeight;
    }
    // getDay(date: Date) {
    //   const dateMoment = moment(date);
    //   this.day = this.cs.getDay(dateMoment.year(), dateMoment.month(), dateMoment.date());
    // }
    async updateByMealCalendar(event: MealCalendar[]) {
      try{
        this.selectedDate = this.calendar.selectedDate;
        const day = event.find(mealCalendar => moment(mealCalendar.date).isSame(moment(this.selectedDate, 'day')))
        if (day) {
          this.meals = day.meals;
          this.mealNutrients = await Promise.all(this.meals.map(async (meal) => {
            const mealNutrients = await this.edamamService.getNurtients(meal.foodId, meal.eatenAmount, meal.measure.uri, meal.qualifier ? meal.qualifier.uri : undefined).toPromise();
            return {
              foodId: meal.foodId, 
              eatenAmount: meal.eatenAmount, 
              measure: meal.measure, 
              qualifier: meal.qualifier, 
              mealNutrients};
          }));
        } else {
          this.meals = [];
          this.mealNutrients = null;
        }
      } catch(error) {
        this.sb.error(error);
        console.error(error);
      }
    }

  async searchMeal(event: Meal) {
    this.router.navigate([`/food-finder`], {queryParams: {q: event.label}});
  }
  async modifyMeal(event: Meal) {

  }
  async removeMeal(event: Meal) {
    try {
      await this.ms.removeMeal(event).toPromise();
      this.mealCalendarUpdate.next([event.eatenDate]);
    } catch(error) {
      console.error(error);
      this.sb.error(error);
    }
  }
  async updateMealType(meal: Meal, type: MealType) {
    try {
      await this.ms.updateMealType(meal, type).toPromise();
      this.mealCalendarUpdate.next([meal.eatenDate]);
    } catch(error) {
      console.error(error);
      this.sb.error(error);
    }
  }
  async duplicateMeal(event: {meal: Meal, date: Date}) {
    try {
      await this.ms.duplicateMeal(this.ls.getUser(), event.meal, event.date).toPromise();
      this.mealCalendarUpdate.next([event.date]);
    } catch(error) {
      console.error(error);
      this.sb.error(error);
    }
  }


    // async getMeal(day?: CalendarDay): Promise<MealCalendar[]> {
      
    //   if (!day || !this.day || !this.day.date) {
    //     this.getDay(moment().toDate());
    //   } else if (!this.cs.isSameDay(day.date, this.day.date)) {
    //     this.getDay(day.date);
    //   }
    //   const start = moment(this.day.date).startOf('day').toDate();
    //   const end = moment(this.day.date).endOf('day').toDate();
    //   const mc  = await this.ms.getMealCalendarByDateRange(this.ls.getUserSession().user, start, end).toPromise();
    //   if (mc && mc.length>0) {
    //     this.meals = mc[0].meals;
    //     this.mealNutrients = await Promise.all(this.meals.map(async (meal) => {
    //       const mealNutrients = await this.edamamService.getNurtients(meal.foodId, meal.eatenAmount, meal.measure.uri, meal.qualifier ? meal.qualifier.uri : undefined).toPromise();
    //       return {
    //         foodId: meal.foodId, 
    //         eatenAmount: meal.eatenAmount, 
    //         measure: meal.measure, 
    //         qualifier: meal.qualifier, 
    //         mealNutrients};
    //     }));
    //     return mc;
    //   } else {
    //     return [];
    //   }
      
    // }
}