import {Component, Input, OnInit, Output, EventEmitter} from "@angular/core";
import { EdamamService } from "../../../../services/edamam.service";
import { EdamamFoodRes, EdamamFood, EdamamNutrientsRes, EdamamMeasure, EdamamFoodHint, MealType } from "../../../../models/edamam.model";

@Component({
    selector: 'food-nutrients',
    templateUrl: './foodNutrients.component.html',
    styleUrls: ['./foodNutrients.component.css']
})
export class FoodNutrientsComponent implements OnInit {
    constructor(private edamamService: EdamamService) {}
    @Input() food: EdamamFoodHint;
    @Input() quantity: number;
    @Input() measurement: EdamamMeasure;
    @Input() nutrients: EdamamNutrientsRes;
    @Input() calendarViewOpen: boolean;
    // @Input() canCreateMeal: boolean;
    @Output() updateNutritionInfo: EventEmitter<{quantity: number, measurement: EdamamMeasure}> = new EventEmitter<{quantity: number, measurement: EdamamMeasure}>();
    @Output() openCreateMealModal: EventEmitter<{food: EdamamFoodHint, quantity: number, measure: string}> = new EventEmitter<{food: EdamamFoodHint, quantity: number, measure: string}>();
    @Output() addMeal: EventEmitter<{food: EdamamFoodHint, type: MealType, quantity: number, measure: EdamamMeasure, qualifier?: EdamamMeasure}> = new EventEmitter<{food: EdamamFoodHint, type: MealType, quantity: number, measure: EdamamMeasure, qualifier?: EdamamMeasure}>();
    // @Output() createMeal: EventEmitter<{food: {food: EdamamFood, measures: EdamamMeasure[]}, quantity: number, measure: string}> = new EventEmitter<{food: {food: EdamamFood, measures: EdamamMeasure[]}, quantity: number, measure: string}>();
    ngOnInit() {
      
    }
    openCreateMeal() {
        this.openCreateMealModal.emit({food: this.food, quantity: this.quantity, measure: this.measurement.uri});
    }
    createMeal(mealType: MealType) {
        this.addMeal.emit({food: this.food, type: mealType, quantity: this.quantity, measure: this.measurement});
    }
    imageNotFound() {
        this.food.food.image = '/assets/imgs/favicon.png';
    }
}