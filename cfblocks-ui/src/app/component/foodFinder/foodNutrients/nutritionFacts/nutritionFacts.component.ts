import {Component, Input, OnInit, Output, SimpleChanges, EventEmitter} from "@angular/core";
import { EdamamNutrientsRes, KcalLabel, EdamamMeasure, EdamamFood, EdamamFoodHint, MealType } from "../../../../../models/edamam.model";
import { FormBuilder, Validators } from "@angular/forms";

@Component({
    selector: 'nutrition-facts',
    templateUrl: './nutritionFacts.component.html',
    styleUrls: ['./nutritionFacts.component.css']
})
export class NutritionFactsComponent implements OnInit {
  constructor(private fb: FormBuilder) {}
  @Input() food: EdamamFoodHint;
  @Input() quantity: number;
  @Input() measurement: EdamamMeasure;
  @Input() nutrients: EdamamNutrientsRes;
  @Input() static: boolean;
  @Input() calendarViewOpen: boolean;
  @Input() mode: string;
  @Output() updateNutritionInfo: EventEmitter<{quantity: number, measurement: EdamamMeasure}> = new EventEmitter<{quantity: number, measurement: EdamamMeasure}>();
  @Output() openCreateMeal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() createMeal: EventEmitter<string> = new EventEmitter<MealType>();
  helpText=`To begin adding a food to your meal plan, first find a food you would like to add. Then select the "Open Calendar" `+
    `button to view the date you would like to add to. Use the Macro calculator above the calendar with the serving quantity ` + 
    `and measurement fields to find an approperate amount. Next use either of the "Create Meal" dropdown menus to select a meal ` + 
    `time of the day and the meal will be added to your meal plan. For more information, please see the resources section.`
  measurementForm = this.fb.group({
    measure: ['', Validators.required],
    quantity: ['', Validators.required]
  });
  get formMeasure() {
    return this.measurementForm.get('measure');
  }
  get formQuantity() {
    return this.measurementForm.get('quantity');
  }
  get updateMode() {
    return this.mode === 'update'  ;
  }
  get foodFinderMode() {
    return this.mode === 'foodfinder';
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.quantity && changes.quantity.currentValue) {
      this.measurementForm.patchValue({quantity: changes.quantity.currentValue});
    }
    if (changes.measurement && changes.measurement.currentValue) {
      this.measurementForm.patchValue({measure: changes.measurement.currentValue.uri});
    }
  }

  ngOnInit() {
    this.formQuantity.valueChanges.subscribe(newQuantity => {
      this.updateNutritionInfo.emit({quantity: newQuantity, measurement: this.food.measures.find(m => m.uri === this.formMeasure.value)});
    });
    this.formMeasure.valueChanges.subscribe(newMeasure => {
      this.updateNutritionInfo.emit({quantity: this.formQuantity.value, measurement: this.food.measures.find(m => m.uri === newMeasure)});
    });
  }

  get listOfExtraMinerals() {
    if (!this.nutrients) {
      return [];
    } else {
      const filterNutrients = ['ENERC_KCAL', 'FAT', 'FASAT', 'FATRN', 'FAMS', 'FAPU', 'CHOCDF:', 'FIBTG', 'SUGAR:', 'PROCNT', 'CHOLE', 'NA'];
      return Object.keys(this.nutrients.totalNutrients).filter(n => !filterNutrients.some(nToFilter => n === nToFilter)).sort((a: string, b: string) => {
        return a <= b ? 1 : -1;
      });
    }
  }

  knownLabelException(label: string): boolean {
    if (label) {
      const exceptions = ['Whole'];
      return exceptions.some(exception => exception === label);
    } else {
      return false;
    }
  }

  getTotalNutrient(nutrient) {
    if (this.nutrients && this.nutrients.totalNutrients) {
      return this.nutrients.totalNutrients[nutrient];
    } else {
      return undefined;
    }
    
  }

  getDailyNutrient(nutrient) {
    if (this.nutrients && this.nutrients.totalDaily) {
      return this.nutrients.totalDaily[nutrient];
    } else {
      return undefined;
    }
    
  }
  createBreakfast() {this.createMeal.emit(MealType.BREAKFAST);};
  createLunch() {this.createMeal.emit(MealType.LUNCH);};
  createDinner() {this.createMeal.emit(MealType.DINNER);};
  createSnack() {this.createMeal.emit(MealType.SNACK);};
  createTeatime() {this.createMeal.emit(MealType.TEATIME);};
}