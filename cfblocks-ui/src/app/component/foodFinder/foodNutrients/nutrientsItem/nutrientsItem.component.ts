import {Component, Input, OnInit} from "@angular/core";
import { KcalLabel } from "../../../../../models/edamam.model";

@Component({
    selector: 'nutrients-item',
    templateUrl: './nutrientsItem.component.html'
})
export class NutrientsItemComponent implements OnInit {
    constructor() {}
    @Input() totalNutrients: KcalLabel;
    @Input() totalDaily: KcalLabel;
    ngOnInit() {
      
    }
}