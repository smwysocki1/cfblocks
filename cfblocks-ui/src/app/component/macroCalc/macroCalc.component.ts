import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import { EdamamService } from "../../../services/edamam.service";
import { EdamamFoodRes, EdamamFood, EdamamNutrientsRes, EdamamMeasure } from "../../../models/edamam.model";
import { Meal } from "../../../models/meal.module";
import { MealService } from "../../../services/meal.service";
import { BlockCalculatorService } from "../../../services/block-calculator.service";
import { LoginService } from "../../../services/login.service";
import { UtilService } from "../../../services/util.service";

@Component({
    selector: 'macro-calculator',
    templateUrl: './macroCalc.component.html',
    styleUrls: ['./macroCalc.component.css']
})
export class MacroCalcComponent implements OnInit {
    constructor(private edamamService: EdamamService, private ms: MealService, private bsc: BlockCalculatorService, private ls: LoginService, private utils: UtilService) {}
    @Input() meals: Meal[];
    @Input() food: {food: EdamamFood, measures: EdamamMeasure[]};
    @Input() nutrients: EdamamNutrientsRes;
    @Input() mealNutrients: any[];
    @Input() quantity: number;
    @Input() measurement: EdamamMeasure;
    @Input() qualifier: EdamamMeasure;
    t1 = 20;
    t2= 40;
    goals
    progressBarMode = 'buffer';
    get progressBar() {
      if (this.goals && this.goals.calories) {
        const mealCals = this.getMealCalories();
        if (mealCals > 0) {
          // if (this.goals.calories <= mealCals) {
            // return 100;
          // } else {
            return mealCals / this.goals.calories * 100;
          // }
        } else return 0;
      } else return 0;
    }
    get progressBarBuffer() {
      if (this.nutrients && this.nutrients && this.nutrients.totalNutrients && this.nutrients.totalNutrients.ENERC_KCAL) {
        const progressBarBuffer = ((this.nutrients.totalNutrients.ENERC_KCAL.quantity) / this.goals.calories) * 100
        // if ((progressBarBuffer) > 100) {
          // return 100;
        // } else {
          return (progressBarBuffer);
          // return ((mealCals + this.nutrients.calories) / this.goals.calories) * 100;
        // }
      } else return 0;
    }
    ngOnInit() {
      this.goals = this.bsc.getGoals(this.ls.getUser());
    }
    getMealCarbs() {
      if (this.nutrients && this.meals && this.mealNutrients) {
        let mealCarbs = 0;
        this.meals.forEach(meal => {
          const mealNutrients = this.mealNutrients.find(mn => mn.foodId == meal.foodId && mn.eatenAmount === meal.eatenAmount && 
            mn.measure.uri === meal.measure.uri && (meal.qualifier ? meal.qualifier.uri = mn.qualifier.uri : true));
          if (mealNutrients && mealNutrients.mealNutrients &&  mealNutrients.mealNutrients.totalNutrients.CHOCDF)
            mealCarbs += mealNutrients.mealNutrients.totalNutrients.CHOCDF.quantity;
        });
        // if (this.nutrients.totalNutrients.CHOCDF.quantity)
        //   mealCarbs += this.nutrients.totalNutrients.CHOCDF.quantity;
        return mealCarbs;
      } else {
        return 0;
      }
    }
    getMealFats() {
      if (this.nutrients && this.meals && this.mealNutrients) {
        let mealFats = 0;
        this.meals.forEach(meal => {
          const mealNutrients = this.mealNutrients.find(mn => mn.foodId == meal.foodId && mn.eatenAmount === meal.eatenAmount && 
            mn.measure.uri === meal.measure.uri && (meal.qualifier ? meal.qualifier.uri = mn.qualifier.uri : true));
          if (mealNutrients && mealNutrients.mealNutrients &&  mealNutrients.mealNutrients.totalNutrients.FAT)
          mealFats += mealNutrients.mealNutrients.totalNutrients.FAT.quantity;
        });
        // if (this.nutrients.totalNutrients.FAT)
        //   mealFats += this.nutrients.totalNutrients.FAT.quantity;
        return mealFats;
      } else {
        return 0;
      }
    }
    getMealProtein() {
      if (this.nutrients && this.meals && this.mealNutrients) {
        let mealProtein = 0;
        this.meals.forEach(meal => {
          const mealNutrients = this.mealNutrients.find(mn => mn.foodId == meal.foodId && mn.eatenAmount === meal.eatenAmount && 
            mn.measure.uri === meal.measure.uri && (meal.qualifier ? meal.qualifier.uri = mn.qualifier.uri : true));
          if (mealNutrients && mealNutrients.mealNutrients && mealNutrients.mealNutrients.totalNutrients.PROCNT)
          mealProtein += mealNutrients.mealNutrients.totalNutrients.PROCNT.quantity;
        });
        // if (this.nutrients.totalNutrients.PROCNT)
        //   mealProtein += this.nutrients.totalNutrients.PROCNT.quantity;
        return mealProtein;
      } else {
        return 0;
      }
    }
    getMealCalories() {
      if (this.nutrients && this.meals && this.mealNutrients) {
        let mealCalories = 0;
        this.meals.forEach(meal => {
          const mealNutrients = this.mealNutrients.find(mn => mn.foodId == meal.foodId && mn.eatenAmount === meal.eatenAmount && 
            mn.measure.uri === meal.measure.uri && (meal.qualifier ? meal.qualifier.uri = mn.qualifier.uri : true));
          if (mealNutrients && mealNutrients.mealNutrients && mealNutrients.mealNutrients.totalNutrients && mealNutrients.mealNutrients.totalNutrients.ENERC_KCAL) {
            mealCalories += mealNutrients.mealNutrients.totalNutrients.ENERC_KCAL.quantity;
          }
        });
        // if (this.nutrients.calories)
        //   mealCalories += this.nutrients.calories;
        return mealCalories;
      } else {
        return 0;
      }
    }


    getRemainingCarbs() {
      if (this.goals && this.goals.carbs) {
        return this.goals.carbs - this.getMealCarbs();
      } else return 0;
    }
    getRemainingFats() {
      if (this.goals && this.goals.carbs) {
        return this.goals.fats - this.getMealFats();
      } else return 0;
    }
    getRemainingProtein() {
      if (this.goals && this.goals.carbs) {
        return this.goals.protein - this.getMealProtein();
      } else return 0;
    }
    getRemainingCalories() {
      if (this.goals && this.goals.carbs) {
        return this.goals.calories - this.getMealCalories();
      } else return 0;
    }

  }