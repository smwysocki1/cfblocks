import {Component, Input, OnInit} from "@angular/core";

@Component({
  selector: 'help',
  templateUrl: './helpTooltip.component.html',
  styleUrls: ['./helpTooltip.component.css']
})
export class HelpTooltipComponent {
  @Input() text: string;
  constructor(){ }
}