import {Component, OnInit, ViewChild, Pipe, PipeTransform} from "@angular/core";
import { MealType } from "../../../models/edamam.model";
import { Meal } from "../../../models/meal.module";
import defaultMeals from '../../../app/data/defaultMeals.json';
import { MealService } from "../../../services/meal.service";
import { MatButtonToggleGroup } from "@angular/material/button-toggle";
import { LoginService } from "../../../services/login.service";
import { User } from "../../../models/user.model";
import { SnackBarService } from "../../../services/snackbar.service";
import * as moment from "moment-timezone";
moment.tz.setDefault('Etc/UTC');

export class DefaultMeal {
  plans: string[];
  type: MealType;
  size?: string;
  meals: Meal[];
}
export class DefaultMealTab {
  label: string;
  plan: DefaultMeal[];
  nutritionPlan: string;
}

@Pipe({ name: 'mealSize' })
export class MealSizePipe implements PipeTransform {
  transform(value: DefaultMeal[], mealSize: string): DefaultMeal[] {
    return value.filter((defaultMeal:DefaultMeal) => !defaultMeal.size || defaultMeal.size === mealSize) as DefaultMeal[];
  }
}
@Pipe({ name: 'mealType' })
export class MealTypePipe implements PipeTransform {
  transform(value: DefaultMeal[], mealType: string): DefaultMeal[] {
    return value.filter((defaultMeal:DefaultMeal) => defaultMeal.type === mealType) as DefaultMeal[];
  }
}
@Component({
    selector: 'default-meals',
    templateUrl: './defaultMeals.component.html',
    styleUrls: ['./defaultMeals.component.css']
})
export class DefaultMealsComponent implements OnInit {
  mealPlanSize:string = 'Small';
  defaultMeals: DefaultMeal[] = defaultMeals as DefaultMeal[];
  user: User;
  tabs: DefaultMealTab[] = [
    {label: 'General', plan: this.general, nutritionPlan: 'calorie-tracking'},
    {label: 'Zone', plan: this.zone, nutritionPlan: 'zone'},
    {label: 'Vegan', plan: this.vegan, nutritionPlan: 'vegan-zone'},
    {label: 'Keto', plan: this.keto, nutritionPlan: 'keto'}
  ];
  selectedTabIndex: number = null;
  constructor(private mealService: MealService, private ls: LoginService, private sbs: SnackBarService) {}
  ngOnInit() { 
    this.user = this.ls.getUser();
    if (this.user && this.user.nutritionPlan) {
      this.selectedTabIndex = this.tabs.findIndex(tab => tab.nutritionPlan === this.user.nutritionPlan);
    }
  }
  displayMealPlanSize(mealPlanSize: string) {
    switch(mealPlanSize) {
      case 'Small': return 'Small - BMR < 1800 kcal';break;
      case 'Medium': return 'Medium - BMR 1800 - 2200 kcal';break;
      case 'Large': return 'Large - BMR 2200+ kcal';break;
    }


  }
  // get tabs(): DefaultMealTab[] {
  //   return [
  //     {label: 'General', plan: this.general},
  //     {label: 'Zone', plan: this.zone},
  //     {label: 'Vegan', plan: this.vegan},
  //     {label: 'Keto', plan: this.keto}
  //   ];
  // }
  get general(): DefaultMeal[]{
    return this.defaultMeals.filter((dm:DefaultMeal) => dm.plans.some(plan => plan === 'General'));
    // return defaultMeals.filter((dm:DefaultMeal) => {!dm.plans.some(plan => plan === 'General');});
  }
  get zone(): DefaultMeal[]{
    return this.defaultMeals.filter((dm:DefaultMeal) => dm.plans.some(plan => plan === 'Zone'));
  }
  get vegan(): DefaultMeal[]{
    return this.defaultMeals.filter((dm:DefaultMeal) => dm.plans.some(plan => plan === 'Vegan'));
  }
  get keto(): DefaultMeal[]{
    return this.defaultMeals.filter((dm:DefaultMeal) => dm.plans.some(plan => plan === 'Keto'));
  }
  // get mealTypes(): string[] {
  //   return Object.keys(MealType);
  //   // return ['Breakfast', 'Lunch', 'Dinner', 'Snack', 'Teatime'];
  // }
  mealTypes = ['Breakfast', 'Lunch', 'Dinner', 'Snack', 'Teatime'];
  getMealDisplay(meal: Meal) {
    // return this.formatName(`${meal.eatenAmount} ${meal.measure.label} ${meal.label}`);
    return this.mealService.getMealDisplay(meal);
  }
  setMealPlanSize(size: string) {
    this.mealPlanSize = size;
  }
  async addMeals(meals: Meal[], event) {
    try {
      if (meals && meals.length > 0) {
        const date = event.value;
        await Promise.all(meals.map(async meal => {
          return await this.mealService.createMeal(this.ls.getUser(), meal.foodId, meal.type, meal.label, date, meal.eatenAmount, meal.measure, meal.qualifier).toPromise();
        }));
        this.sbs.open(`Successfully added ${meals[0].type} to ${moment(date).format('MMMM Do, YYYY')}`);
      }
      event.target.value = undefined;
    } catch(error) {
      this.sbs.error(error);
      console.error(error);
    }
  }
    
}