import {Component, Input, OnInit, Output, Pipe, PipeTransform, EventEmitter, SimpleChanges} from '@angular/core';
import {CalendarDay, CalendarMonth, CalendarWeek} from './calendar.model';
import {CalendarService} from './calendar.service';
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');
import {Meal, MealCalendar} from '../../../models/meal.module';
import {MealService} from '../../../services/meal.service';
import {Router} from '@angular/router';
import {LoginService} from '../../../services/login.service';
import { Observable, Subscription } from 'rxjs';
import { MealType } from '../../../models/edamam.model';
import { SnackBarService } from '../../../services/snackbar.service';

@Component({
  selector: 'calendar',
  templateUrl: './calendar.html',
  styles: [`
    .meal-calendar-heading {
      position: sticky;
      position: -webkit-sticky;
      top: 58px;
      z-index: 1;
      background: rgba(255,255,255,0.8);
    }
    .meal-calendar-body.card-body {
      padding:.3em;
    }
  `]
})
export class CalendarComponent implements OnInit {
  @Input() name: string;
  @Input() view = 'Bi-Weekly';
  @Input() mealUpdateNotice:Observable<Date>;
  @Input() selectedDate: Date;
  @Output() dateChange: EventEmitter<Date> = new EventEmitter<Date>();
  @Output() mealCalendarUpdate: EventEmitter<MealCalendar[]> = new EventEmitter<MealCalendar[]>();
  @Output() searchMeal: EventEmitter<Meal> = new EventEmitter<Meal>();
  @Output() modifyMeal: EventEmitter<Meal> = new EventEmitter<Meal>();
  @Output() removeMeal: EventEmitter<Meal> = new EventEmitter<Meal>();
  @Output() updateMealType: EventEmitter<{meal:Meal, type: MealType}> = new EventEmitter<{meal:Meal, type: MealType}>();
  @Output() duplicateMeal: EventEmitter<{meal: Meal, date: Date}> = new EventEmitter<{meal: Meal, date: Date}>();
  mealUpdateNoticeSub: Subscription;
  daysOfWeek: string[] = [];
  monthsOfYear: string[] = [];
  month: CalendarMonth = new CalendarMonth();
  mealCalendar: MealCalendar[] = [];
  helpText: string = `Here is a global view of your meal plan. In the toolbar here, you may change ` + 
    `the view to Day, Week, bi-weekly, or month to see more or less dates at a time. The Current Date ` + 
    `will always have a slight yellow color while the selected date will be a green color with a red heading. ` + 
    `Selecting the currently selected date a second time will view that day in the single day view. from ` + 
    `the day view, you find the actions elipse with a dropdown menu that you can adjust the items displayed ` + 
    `on that day. For more information, please see the resources page. `;
  constructor(private cs: CalendarService, private mealService: MealService, private router: Router, private ls: LoginService, private sb: SnackBarService) { }
  ngOnInit() {
    this.daysOfWeek = this.cs.getDaysOfWeek();
    this.monthsOfYear = this.cs.getMonthsOfYear();
    this.selectDate(moment().startOf('day').toDate());
    this.getMealCalendar();
    if (this.mealUpdateNotice) {
      this.mealUpdateNoticeSub = this.mealUpdateNotice.subscribe((event: Date) => {
        if(event) {
          this.getMealCalendar();
        }
      });
    }
  }

  datepickerDateSelection(event) {
    if (!this.isSameDay(event.value, this.selectedDate)) {
      this.selectDate(event.value);
    }
  }

  ngOnDestroy() {
    if (this.mealUpdateNoticeSub) {
      this.mealUpdateNoticeSub.unsubscribe();
    }
  }

  goToDay(dateOfMonth: Date) {
    const year = moment(dateOfMonth).year();
    const month = moment(dateOfMonth).month();
    const day = moment(dateOfMonth).day();
    const m = this.cs.getMonth(year, month);
    m.weeks = m.weeks.filter(week => this.isSameDay(week.days[0].date, moment(dateOfMonth).startOf('week').toDate())) as [CalendarWeek];
    m.weeks = m.weeks.map(week => {
      week.days = week.days.filter(day => moment(day.date).isSame(moment(dateOfMonth), 'day'));
      return week;
    })
    this.month = m;
    this.getMealCalendar();
  }
  goToMonth(dateOfMonth: Date) {
    const year = moment(dateOfMonth).year();
    const month = moment(dateOfMonth).month();
    this.month = this.cs.getMonth(year, month);
    this.getMealCalendar();
  }
  goToBiWeekly(dateOfMonth: Date) {
    const year = moment(dateOfMonth).year();
    const month = moment(dateOfMonth).month();
    const m = this.cs.getMonth(year, month);
    m.weeks = m.weeks.filter(week => this.isSameDay(week.days[0].date, moment(dateOfMonth).startOf('week').toDate()) ||
      this.isSameDay(week.days[0].date, moment(dateOfMonth).startOf('week').add(7, 'days').toDate())
    ) as [CalendarWeek];
    this.month = m;
    this.getMealCalendar();
  }
  goToWeekly(dateOfMonth: Date) {
    const year = moment(dateOfMonth).year();
    const month = moment(dateOfMonth).month();
    const m = this.cs.getMonth(year, month);
    m.weeks = m.weeks.filter(week => this.isSameDay(week.days[0].date, moment(dateOfMonth).startOf('week').toDate())) as [CalendarWeek];
    this.month = m;
    this.getMealCalendar();
  }
  goBackMonth() {
    if (this.view === 'Day') {
      this.selectedDate = moment(this.selectedDate).subtract(1, 'day').toDate();
      this.goToDay(this.selectedDate);
    } else if (this.view === 'Weekly') {
      this.selectedDate = moment(this.selectedDate).subtract(1, 'weeks').toDate();
      this.goToWeekly(this.selectedDate);
    } else if (this.view === 'Bi-Weekly') {
      this.selectedDate = moment(this.selectedDate).subtract(1, 'weeks').toDate();
      this.goToBiWeekly(this.selectedDate);
    } else if (!this.view || this.view === 'Monthly') {
      this.selectedDate = moment(this.selectedDate).subtract(1, 'months').toDate();
      this.goToMonth(this.selectedDate);
    }
  }
  goNextMonth() {
    if (this.view === 'Day') {
      this.selectedDate = moment(this.selectedDate).add(1, 'day').toDate();
      this.goToDay(this.selectedDate);
    } else if (this.view === 'Weekly') {
      this.selectedDate = moment(this.selectedDate).add(1, 'weeks').toDate();
      this.goToWeekly(this.selectedDate);
    } else if (this.view === 'Bi-Weekly') {
      this.selectedDate = moment(this.selectedDate).add(1, 'weeks').toDate();
      this.goToBiWeekly(this.selectedDate);
    } else if (!this.view || this.view === 'Monthly') {
      this.selectedDate = moment(this.selectedDate).add(1, 'months').toDate();
      this.goToMonth(this.selectedDate);
    }
  }
  toggleToday(): void {
    if (!this.isSameDay(moment().toDate(), this.selectedDate)) {
      this.selectDate(moment().toDate());
    }
  }
  selectDate(date: Date) {
    if (!this.selectedDate || !this.isSameDay(date, this.selectedDate)) {
      this.selectedDate = date;
      if (!this.cs.dateInCalendarMonth(this.month, this.selectedDate)) {
        if (!this.view || this.view === 'Monthly') {
          this.goToMonth(this.selectedDate);
        } else if (this.view === 'Weekly') {
          this.goToWeekly(this.selectedDate);
        } else if (this.view === 'Bi-Weekly') {
          this.goToBiWeekly(this.selectedDate);
        } else if (this.view === "Day") {
          this.goToDay(this.selectedDate);
        }
      }
    } else {
      if (this.selectedDate && !this.isDayView) {
        this.loadView('Day');
        // this.router.navigate(['/meal-builder', moment(this.selectedDate).format('MMDDYY')]);
      }
    }
  }
  isSameDay(date1: Date, date2: Date) {
    return this.cs.isSameDay(date1, date2);
  }
  get todayIsSelected() {
    return this.isSameDay(this.selectedDate, moment().toDate());
  }
  getMeal(day: CalendarDay): Meal[] {
    // if (this.cs.isSameDay(day.date, moment().toDate())) {
    //   console.log(this.mealCalendar);
    //   console.log(day.date);
    //   this.mealCalendar.forEach(m => {
    //     console.log(m.date);
    //     if (this.cs.isSameDay(day.date, moment(m.date).toDate())) {
    //       console.log(m);
    //     }
    //   });
    // }
    if (day) {
      const mealCalendar = this.mealCalendar.find(meal => moment(meal.date).isSame(day.date, 'day'));
      if (mealCalendar) {
        return mealCalendar.meals;
      }
    }
    return [];
  }
  async getMealCalendar(start?: Date, end?: Date, doNotRepeat?: boolean) {
    if (start && end) {
      // this.mealService.getMealCalendar(start, end).subscribe(mealCalendar => {
      //   this.mealCalendar = mealCalendar;
      // });
      // if (this.ls.getUserSession().authenticated) {
      try {
        const mc = await this.mealService.getMealCalendarByDateRange(this.ls.getUserSession().user, start, end).toPromise();
        if (mc) {
          this.mealCalendar = mc;
          this.mealCalendarUpdate.emit(this.mealCalendar);
        }
      } catch(err) {
        console.error(err);
        this.sb.error(err.message);
      }
          // } else {
          //   this.mealCalendar.forEach(mealCalendar => {
          //     mealCalendar.date = date;
          //   });
          //   this.mealCalendar.date = date;
          //   this.mealCalendar.user = this.user.id;
          // }
        
      // }
    } else {
      this.month.weeks.forEach(week => {
        week.days.forEach(day => {
          if (moment(day.date).isBefore(moment(start)) || start === undefined) {
            start = day.date;
          }
          if (moment(day.date).isAfter(moment(end)) || end === undefined) {
            end = day.date;
          }
        });
      });
      if (doNotRepeat) {
        console.error('could not find start and end date of meal planner');
      } else {
        this.getMealCalendar(start, end, true);
      }
    }
  }
  get isDayView(): boolean {return this.view === 'Day';}
  get isWeekView(): boolean {return this.view === 'Weekly';}
  get isBiWeekView(): boolean {return this.view === 'Bi-Weekly';}
  get isMonthView(): boolean {return this.view === 'Monthly';}
  loadView(view) {
    this.view = view;
    if (this.view === 'Weekly') {
      this.goToWeekly(this.selectedDate);
    } else if (this.view === 'Bi-Weekly') {
      this.goToBiWeekly(this.selectedDate);
    } else if (!this.view || this.view === 'Monthly') {
      this.goToMonth(this.selectedDate);
    } else if(this.view === 'Day') {
      this.goToDay(this.selectedDate);
      // this.router.navigate(['/meal-builder', moment(this.selectedDate).format('MMDDYY')]);
    }
  }
  searchMealEvent(event: Meal) {this.searchMeal.emit(event);}
  modifyMealEvent(event: Meal) {this.modifyMeal.emit(event);}
  removeMealEvent(event: Meal) {this.removeMeal.emit(event);}
  getGroceryList() {
    let startDate = moment().toDate();
    let endDate = moment().toDate();
    switch(this.view) {
      case 'Weekly': 
        startDate = moment(this.selectedDate).startOf('week').toDate();
        endDate = moment(this.selectedDate).endOf('week').toDate();
        break;
      case 'Bi-Weekly': 
        startDate = moment(this.selectedDate).startOf('week').toDate();
        endDate = moment(this.selectedDate).add(7,'day').endOf('week').toDate();
        break;
      case 'Monthly': 
        startDate = moment(this.selectedDate).startOf('month').toDate();
        endDate = moment(this.selectedDate).endOf('month').toDate();
        break;
      case 'Day': 
        startDate = moment(this.selectedDate).startOf('day').toDate();
        endDate = moment(this.selectedDate).endOf('day').toDate();
        break;
      default: 
        startDate = moment(this.selectedDate).startOf('day').toDate();
        endDate = moment(this.selectedDate).endOf('day').toDate();
        break;
    }
    // this.router.navigate([`/food-finder`], {queryParams: {q: event.label}});
    this.router.navigate(['/grocery-list'], {queryParams: {startDate,endDate}});

  }
}
