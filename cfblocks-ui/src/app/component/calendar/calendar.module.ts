// import {NgModule} from '@angular/core';
// import {BrowserModule} from '@angular/platform-browser';
// import {CalendarComponent} from './calendar.component';
// import {CalendarService} from './calendar.service';
// import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// import {DayComponent, MealUpdateDialogComponent} from './day/day.component';
// import {MomentPipeModule} from '../../pipe/moment.pipe';
// import {MealSortPipeModule} from '../../pipe/mealSort.pipe';
// import { MatButtonModule } from '@angular/material/button';
// import { MatIconModule } from '@angular/material/icon';
// import {MatMenuModule} from '@angular/material/menu';
// import { MatDatepickerModule } from '@angular/material/datepicker';
// import { MatCardModule } from '@angular/material/card';
// import { MatDialogModule } from '@angular/material/dialog';
// import { FoodNutrientsComponent } from '../foodFinder/foodNutrients/foodNutrients.component';

// @NgModule({
//   declarations: [
//     CalendarComponent,
//     DayComponent,
//     MealUpdateDialogComponent
//   ],
//   imports: [
//     BrowserModule,
//     ReactiveFormsModule,
//     FormsModule,
//     MomentPipeModule,
//     MealSortPipeModule,
//     MatButtonModule,
//     MatIconModule,
//     MatMenuModule,
//     MatDatepickerModule,
//     MatCardModule,
//     MatDialogModule
//   ],
//   exports: [CalendarComponent, DayComponent, MealUpdateDialogComponent],
//   providers: [CalendarService],
//   entryComponents: [MealUpdateDialogComponent, FoodNutrientsComponent]
// })
// export class CalendarModule { }
