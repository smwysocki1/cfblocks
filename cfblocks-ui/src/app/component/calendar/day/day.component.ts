import {Component, EventEmitter, Inject, Input, Output, Pipe, PipeTransform, SimpleChanges, ViewChild} from '@angular/core';
import {CalendarDay} from '../calendar.model';
import {Meal} from '../../../../models/meal.module';
import {MealService} from '../../../../services/meal.service';
import {ValidationService} from '../../../../services/validation.service';
import {Router} from '@angular/router';
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');
declare var $: any;
import {CalendarService} from '../calendar.service';
import { EdamamCaution, EdamamFoodHint, EdamamMeasure, EdamamNutrientsRes, MealType } from '../../../../models/edamam.model';
import { EdamamService } from '../../../../services/edamam.service';
import { SnackBarService } from '../../../../services/snackbar.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
export class DialogData {
  food: EdamamFoodHint;
  quantity: number;
  measurement: EdamamMeasure;
  qualifier: EdamamMeasure;
  nutrients: EdamamNutrientsRes;
}
export class MealPlan {
  BREAKFAST: Meal[];
  LUNCH: Meal[];
  DINNER: Meal[];
  SNACK: Meal[];
  TEATIME: Meal[];
}
@Component({
  selector: 'calendar-day',
  templateUrl: './day.html',
  styles: [`
    .day {
      opacity: .25;
    }
    .monthly-view > .day,
    .bi-weekly-view > .day,
    .weekly-view > .day {
      height: 0;
      padding-bottom: 100%;
      width: 100%;
      min-width: 135px;
      // overflow: overlay;
    }
    .day.valid-month {
      opacity: 1;
    }
    .breakfast, .lunch, .dinner, .snack { }
    .add-meal {
      display:inline-block;
      padding:6px;
      opacity: 0.3;
    }
    .add-meal:hover {
      opacity:1;
    }
    .meal-text {
      padding:0 .5em;
    }
    h5 {
      margin-bottom: 0;
    }
    .monthly-view .is-selected,
    .bi-weekly-view .is-selected,
    .weekly-view .is-selected {
      background-color: #ccff99;
    }
    .is-selected .dayNumber {
      font-weight: bolder;
      text-decoration: underline;
    }
    .mat-standard-chip {    
      /* height: auto !important; */
      font-size: 10px;
      min-height: 20px;
    }
    .mat-standard-chip.mat-chip-with-trailing-icon {
      padding: 5px;
    }
    .mat-standard-chip .mat-icon{
      font-size: 10px;
      width: 10px;
      height: 10px;
      margin-left: 4px;
    }
    .mat-standard-chip.mat-chip-with-trailing-icon {
      margin: 4px 2px;
    }
  `]
})
export class DayComponent {
  @Input() day: CalendarDay;
  @Input() meals: [Meal];
  @Input() isSelected: boolean;
  @Input() view: string;
  @Output() updateSelectedDate: EventEmitter<Date> = new EventEmitter<Date>();
  @Output() updateSelectedMeal: EventEmitter<Meal> = new EventEmitter<Meal>();
  @Output() searchMeal: EventEmitter<Meal> = new EventEmitter<Meal>();
  @Output() modifyMeal: EventEmitter<Meal> = new EventEmitter<Meal>();
  @Output() removeMeal: EventEmitter<Meal> = new EventEmitter<Meal>();
  @Output() updateMealType: EventEmitter<{meal:Meal, type: MealType}> = new EventEmitter<{meal:Meal, type: MealType}>();
  @Output() duplicateMeal: EventEmitter<{meal: Meal, date: Date}> = new EventEmitter<{meal: Meal, date: Date}>();
  mealNutrients: {foodId: string, nutrient: EdamamNutrientsRes}[] = [];
  foods: EdamamFoodHint[] = [] as EdamamFoodHint[];
  

  cautionLabelResults: EdamamCaution[] = [];
  dietLabelResults: {type: string, 'web-label': string, 'api-label': string, definition: string, label?: string}[] = [];
  healthLabelResults: {type: string, 'web-label': string, 'api-label': string, definition: string, label?: string}[] = [];

  constructor(private mealService: MealService, private edamamService: EdamamService, private router: Router, private cs: CalendarService, private sb: SnackBarService,
    public dialog: MatDialog) { }
  async ngOnInit() {
    // $('[data-toggle="tooltip"]').tooltip({html: true});

  //   $('[data-toggle="tooltip"]').tooltip({
  //     // placement: place, 
  //     trigger: 'hover', sanitize: false, sanitizeFn: content => content
  //  });
  await this.getLabels();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.meals && changes.meals.currentValue) {
      if (this.view === 'Day') {
        this.getFoodInfo();
        this.getMealNutrition();
      } else {
      //   $('[data-toggle="tooltip"]').tooltip({
      //     // placement: place, 
      //     trigger: 'hover', sanitize: false, sanitizeFn: content => content
      //  });
      }
    }
  }
  async getFoodInfo() {
    const foodsToFetch = this.meals.filter(m => !this.getFood(m));
    if (foodsToFetch && foodsToFetch.length > 0) {
      const newFoods = await Promise.all(foodsToFetch.map(async meal => {
        return await this.edamamService.getFood(meal.foodId, meal.label).toPromise();
      }));
      this.foods = this.foods.concat(newFoods);
    }
  }
  async getMealNutrition() {
    const nutritionsToFetch = this.meals.filter(m => !this.getNutrients(m));
    if (nutritionsToFetch && nutritionsToFetch.length > 0) {
      const mealNutrition = await Promise.all(nutritionsToFetch.map(async meal => {
        const nutrient = await this.edamamService.getNurtients(meal.foodId, meal.eatenAmount, meal.measure.uri, meal.qualifier ? meal.qualifier.uri : undefined).toPromise();
        return {foodId: meal.foodId, nutrient: nutrient, quantity: meal.eatenAmount, measurement: meal.measure, qualifier: meal.qualifier};
      }));
      this.mealNutrients = this.mealNutrients.concat(mealNutrition);
      // $('[data-toggle="tooltip"]').tooltip({
      //   // placement: place, 
      //   trigger: 'hover', sanitize: false, sanitizeFn: content => content
      // });
    }
  }
  addMeal() {
    this.router.navigate(['/meal-builder', moment(this.day.date).format('MMDDYY')]);
  }
  showMeal() { }
  getMealDisplay(meal: Meal) {
    // return this.formatName(`${meal.eatenAmount} ${meal.measure.label} ${this.getMealDisplay(meal)}`);
    // return `${meal.eatenAmount} ${meal.measure.label} ${this.getMealDisplay(meal)}`;
    return this.mealService.getMealDisplay(meal);
  }
  getNutrients(meal: Meal) {
    if (meal && this.mealNutrients) {
      const nutrient = this.mealNutrients.find((mn: {foodId: string, nutrient: EdamamNutrientsRes, quantity: number, measurement: EdamamMeasure, qualifier?: EdamamMeasure}) => {
        return (mn.foodId === meal.foodId && mn.quantity === meal.eatenAmount && mn.measurement.uri === meal.measure.uri && ((meal.qualifier && mn.qualifier && mn.qualifier.uri === meal.qualifier.uri) || (!meal.qualifier && !mn.qualifier)));
      });
      if (nutrient) {
        return nutrient.nutrient;
      } else return undefined;
    }
  }
  getFood(meal) {
    if (meal && this.foods) {
      const food = this.foods.find((f: EdamamFoodHint) => f.food.foodId === meal.foodId && f.food.label === meal.label);
      return food;
    }
  }
  get breakfastMeals(): Meal[]{ return this.meals.filter(meal => this.mealService.isBreakFast(meal))};
  get lunchMeals(): Meal[]{ return this.meals.filter(meal => this.mealService.isLunch(meal))};
  get dinnerMeals(): Meal[]{ return this.meals.filter(meal => this.mealService.isDinner(meal))};
  get snackMeals(): Meal[]{ return this.meals.filter(meal => this.mealService.isSnack(meal))};
  get teatimeMeals(): Meal[]{ return this.meals.filter(meal => this.mealService.isTeatime(meal))};
  listOfMealTypes = [MealType.BREAKFAST, MealType.LUNCH, MealType.DINNER, MealType.SNACK, MealType.TEATIME];
  get listOfMeals(): MealPlan{ 
    const meals = {};
    meals[MealType.BREAKFAST] = this.breakfastMeals;
    meals[MealType.LUNCH] = this.lunchMeals;
    meals[MealType.DINNER] = this.dinnerMeals;
    meals[MealType.SNACK] = this.snackMeals;
    meals[MealType.TEATIME] = this.teatimeMeals;
    return meals as MealPlan;
  };
  isBreakFast(meal: Meal): boolean {
    return this.mealService.isBreakFast(meal);
  }
  isLunch(meal: Meal): boolean {
    return this.mealService.isLunch(meal);
  }
  isDinner(meal: Meal): boolean {
    return this.mealService.isDinner(meal);
  }
  isSnack(meal: Meal): boolean {
    return this.mealService.isSnack(meal);
  }
  isTeatime(meal: Meal): boolean {
    return this.mealService.isTeatime(meal);
  }
  formatName(name: string) {
    if (name && name.trim() && name.trim().length > 20) {
      return name.trim().substr(0, 17) + '...';
    } else if (name && name.trim()) {
      return name.trim();
    } else {
      return name;
    }
  }
  validMonth(isValid: boolean): boolean {
    if (this.view === 'Monthly') {
      return isValid;
    } else {
      return true;
    }
  }
  selectDate(date: Date) {
    this.updateSelectedDate.emit(date);
  }
  selectMeal(meal: Meal) {
    this.updateSelectedMeal.emit(meal);
  }
  isToday(date: Date) {
    return this.cs.isSameDay(date, moment().toDate());
  }
  search(meal: Meal) {
    this.searchMeal.emit(meal);
  }
  modify(meal: Meal) {
    this.modifyMeal.emit(meal);
    this.sb.open(`${this.getMealDisplay(meal)} has been modified`);
  }
  duplicate(meal: Meal, mealType: MealType, event) {
    const m: Meal = {...meal};
    m.type = mealType;
    this.duplicateMeal.emit({meal: m, date: moment(event.value).toDate()});
    this.sb.open(`${this.getMealDisplay(m)} has been duplicated to ${moment(event.value).format('MMMM Do')}`);
    event.target.value = undefined;
  }
  duplicateMeals(meals: Meal[], event) {
    meals.forEach(meal => {
      this.duplicateMeal.emit({meal, date: moment(event.value).toDate()});
    });
    this.sb.open(`Meals have been duplicated to ${moment(event.value).format('MMMM Do')}`);
    event.target.value = undefined;
  }
  remove(meal: Meal) {
    this.removeMeal.emit(meal);
    this.sb.open(`${this.getMealDisplay(meal)} is removed`);
  }
  removeMeals(meals: Meal[]) {
    meals.forEach(meal => {
      this.removeMeal.emit(meal);
    });
    this.sb.open(`Meals have been removed`);
  }
  moveToBreakfast(meal: Meal) {
    this.updateMealType.emit({meal, type: MealType.BREAKFAST});
    this.sb.open(`Moved ${this.getMealDisplay(meal)} to ${MealType.BREAKFAST}`);
  }
  moveToLunch(meal: Meal) {
    this.updateMealType.emit({meal, type: MealType.LUNCH});
    this.sb.open(`Moved ${this.getMealDisplay(meal)} to ${MealType.LUNCH}`);
  }
  moveToDinner(meal: Meal) {
    this.updateMealType.emit({meal, type: MealType.DINNER});
    this.sb.open(`Moved ${this.getMealDisplay(meal)} to ${MealType.DINNER}`);
  }
  moveToSnack(meal: Meal) {
    this.updateMealType.emit({meal, type: MealType.SNACK});
    this.sb.open(`Moved ${this.getMealDisplay(meal)} to ${MealType.SNACK}`);
  }
  moveToTeatime(meal: Meal) {
    this.updateMealType.emit({meal, type: MealType.TEATIME});
    this.sb.open(`Moved ${this.getMealDisplay(meal)} to ${MealType.TEATIME}`);
  }
  moveMealsToBreakfast(meals: Meal[]) {
    meals.forEach(meal => {
      this.updateMealType.emit({meal, type: MealType.BREAKFAST});
    });
    this.sb.open(`Moved meals to ${MealType.BREAKFAST}`);
  }
  moveMealsToLunch(meals: Meal[]) {
    meals.forEach(meal => {
      this.updateMealType.emit({meal, type: MealType.LUNCH});
    });
    this.sb.open(`Moved meals to ${MealType.LUNCH}`);
  }
  moveMealsToDinner(meals: Meal[]) {
    meals.forEach(meal => {
      this.updateMealType.emit({meal, type: MealType.DINNER});
    });
    this.sb.open(`Moved meals to ${MealType.DINNER}`);
  }
  moveMealsToSnack(meals: Meal[]) {
    meals.forEach(meal => {
      this.updateMealType.emit({meal, type: MealType.SNACK});
    });
    this.sb.open(`Moved meals to ${MealType.SNACK}`);
  }
  moveMealsToTeatime(meals: Meal[]) {
    meals.forEach(meal => {
      this.updateMealType.emit({meal, type: MealType.TEATIME});
    });
    this.sb.open(`Moved meals to ${MealType.TEATIME}`);
  }
  async toggleModifyMeal(meal: Meal): Promise<void> {
    try {
      const m = {...meal};
      const dialogRef = this.dialog.open(MealUpdateDialogComponent, {
        maxWidth: '31em',
        data: m as Meal
      });

      dialogRef.afterClosed().subscribe((result: DialogData) => {
        m.eatenAmount = result.quantity;
        m.measure = result.measurement;
        m.qualifier = result.qualifier;
        this.modify(m);
      });
    } catch(error) {
      this.sb.error(error);
      console.error(error);
    }
  }
  getChip(webLabel: string, type: string) {
    if (type === 'dietLabel') {
      const found = this.dietLabelResults.find(l => l['web-label'] === webLabel);
      if (found && found.label) return found.label;
    } else if (type === 'healthLabel') {
      const found = this.healthLabelResults.find(l => l['web-label'] === webLabel);
      if (found && found.label) return found.label
    } 
    return webLabel;
  }
  async getLabels() {
    try {
      this.cautionLabelResults = await this.edamamService.getCautionLabels().toPromise();
      this.dietLabelResults = this.edamamService.getDietLabels();
      this.healthLabelResults = this.edamamService.getHealthLabels();
    } catch(error) {
      this.sb.error(error);
      console.error(error);
    }
  }
}
@Component({
  selector: 'meal-update-dislog',
  templateUrl: 'meal-update-dialog.html',
})
export class MealUpdateDialogComponent {
  food: EdamamFoodHint;
  quantity: number;
  measurement: EdamamMeasure;
  qualifier: EdamamMeasure;
  nutrients: EdamamNutrientsRes;
  loading = true;
  constructor(
    public dialogRef: MatDialogRef<MealUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public meal: Meal,
    private edamamService: EdamamService,
    private sbs: SnackBarService) {}

  get result(): DialogData { return {food: this.food, quantity: this.quantity, measurement: this.measurement, nutrients: this.nutrients, qualifier: this.qualifier}};
    
  onNoClick(): void {
    this.dialogRef.close();
  }
  async ngOnInit() {
    this.food = await this.edamamService.getFood(this.meal.foodId, this.meal.label).toPromise();
    const qualifier = this.meal.qualifier ? this.meal.qualifier.uri : undefined;
    this.nutrients = await this.edamamService.getNurtients(this.meal.foodId, this.meal.eatenAmount, this.meal.measure.uri, qualifier).toPromise();
    this.quantity = this.meal.eatenAmount;
    this.measurement = this.meal.measure;
    this.qualifier = this.meal.qualifier;
    this.loading = false;
  }
  async updateNutritionInfo(event:{quantity: number, measurement: EdamamMeasure}) {
    if (event.measurement && event.measurement.uri !== this.measurement.uri && event.quantity === this.quantity) {
      event.quantity = 1;
    }
    await this.getNutrients(this.food, event.quantity, event.measurement);
  }
  async getNutrients(food: EdamamFoodHint, quantity?: number, measure?: EdamamMeasure) {
    try {
      this.food = food;
      const foodId = food.food.foodId;
      this.quantity = (quantity === undefined ? 100 : quantity);
      this.measurement = food.measures.find(m => m.uri === (measure ? measure.uri : 'http://www.edamam.com/ontologies/edamam.owl#Measure_gram'));
      this.nutrients = await this.edamamService.getNurtients(foodId, this.quantity, this.measurement.uri, this.qualifier ? this.qualifier.uri : undefined).toPromise();
    } catch(error) {
      this.sbs.error(error);
      console.error(error);
    }
  }
}
