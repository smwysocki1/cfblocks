import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";

@Component({
    selector: 'food-builder',
    templateUrl: './foodBuilder.component.html'
})
export class CalendarComponent implements OnInit {
    constructor(private router: Router) {}
    ngOnInit() {

    }
    startNewFood() {
        this.router.navigate(['/create/rawFood']);
    }
    startNewRecipe() {
        this.router.navigate(['/create/recipe']);
    }
}