import { BrowserModule } from '@angular/platform-browser';
import { NgModule, SecurityContext } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import {NavbarComponent} from './navbar/navbar.component';
import {FooterComponent} from './footer/footer.component';
import {HomeComponent} from './containers/home/home.component';
import {routes} from './app.routes';
import { RouterModule } from '@angular/router';
import {LoginService} from '../services/login.service';
import {ValidationService} from '../services/validation.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NavbarSearchService} from './navbar/navbar-search.service';
// import {AdminComponent} from './containers/admin/admin.component';
import {MealCalendarComponent} from './containers/meal-calendar/meal-calendar.component';
// import {NotificationService} from '../services/notification.service';
import {BlockCalculatorModule} from './containers/block-calculator/block-calculator.module';
// import {CalendarModule} from './component/calendar/calendar.module';
import {MealService} from '../services/meal.service';
import {HelperService} from '../services/helper.service';
import {SignupModule} from './containers/signup/signup.module';
// import {FirebaseService} from '../services/firebase.service';
import {UtilService} from '../services/util.service';
// import {UserAccountModule} from './component/userAccount/userAccount.module';
import {SigninModule} from './containers/signin/signin.module';
// import {MealBuilderModule} from './component/mealBuilder/mealBuilder.module';
// import {FirebaseAbstractionLayerService} from '../services/firebaseAbstractionLayer.service';
import {AppRoutingModule} from './app.routing-module';
import {RequestService} from '../services/request.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NotFoundComponent} from './containers/not-found/not-found.component';
import {PlannerComponent} from './containers/planner/planner.component';
import {MomentPipeModule} from './pipe/moment.pipe';
import {CalendarService} from './component/calendar/calendar.service';
import { FoodAPIService } from '../services/foodAPI.service';
import { EdamamService} from '../services/edamam.service';
import { FoodFinderComponent } from './component/foodFinder/foodFinder.component';
import { FoodResultComponent} from './component/foodFinder/foodResult/foodResult.component';
import { MainMenuComponent } from './containers/main-menu/main-menu.component';
import { FoodNutrientsComponent } from './component/foodFinder/foodNutrients/foodNutrients.component';
import { EdamamMeasurePipeModule } from './pipe/edamamMeasurement.pipe';
import { NutrientsItemComponent } from './component/foodFinder/foodNutrients/nutrientsItem/nutrientsItem.component';
import { NutritionFactsComponent } from './component/foodFinder/foodNutrients/nutritionFacts/nutritionFacts.component';
import { CreateMealModalComponent } from './component/foodFinder/createMealModal/createMealModal.component';
import { MacroCalcComponent } from './component/macroCalc/macroCalc.component';
import { ImageFallbackDirective } from './directives/imgFallback.directive';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from "@angular/material/snack-bar";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {SnackBarService} from "../services/snackbar.service";
import {MatStepperModule} from "@angular/material/stepper";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { MatChipsModule } from '@angular/material/chips';
import { MatMenuModule } from '@angular/material/menu';
import { GroceryListComponent } from './containers/grocery-list/grocery-list.component';
import { MarkdownModule } from 'ngx-markdown';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ResourcesComponent } from './containers/resources/resources.component';
import { ResourceMenuComponent } from './containers/resources/resource-menu/resource-menu.component';
import { DefaultMealsComponent, MealSizePipe, MealTypePipe } from './component/defaultMeals/defaultMeals.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { DayComponent, MealUpdateDialogComponent } from './component/calendar/day/day.component';
import { CalendarComponent } from './component/calendar/calendar.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HelpTooltipComponent } from './component/helpTooltip/helpTooltip.component';
import { ResourceContentComponent } from './containers/resources/resource-content/resource-content.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    // FooterComponent,
    HomeComponent,
    MainMenuComponent,
    // AdminComponent,
    MealCalendarComponent,
    NotFoundComponent,
    PlannerComponent,
    FoodFinderComponent,
    FoodResultComponent,
    FoodNutrientsComponent,
    NutrientsItemComponent,
    NutritionFactsComponent,
    CreateMealModalComponent,
    MacroCalcComponent,
    GroceryListComponent,
    ResourcesComponent,
    DefaultMealsComponent,
    ImageFallbackDirective,
    MealTypePipe,
    MealSizePipe,
    CalendarComponent,
    DayComponent,
    MealUpdateDialogComponent,
    ResourceMenuComponent,
    HelpTooltipComponent,
    ResourceContentComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MarkdownModule.forRoot({
      loader: HttpClient
    }),
    BlockCalculatorModule,
    // CalendarModule,
    SigninModule,
    SignupModule,
    // UserAccountModule,
    // MealBuilderModule,
    MomentPipeModule,
    EdamamMeasurePipeModule,
    MatButtonModule,
    MatIconModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatCardModule,
    MatMomentDateModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatDividerModule,
    MatChipsModule,
    RouterModule,
    MatMenuModule,
    MatSidenavModule,
    MatTabsModule,
    MatListModule,
    MatToolbarModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatTooltipModule
  ],
  providers: [
    LoginService,
    ValidationService,
    NavbarSearchService,
    // NotificationService,
    MealService,
    HelperService,
    // FirebaseService,
    // FirebaseAbstractionLayerService,
    UtilService,
    RequestService,
    CalendarService,
    FoodAPIService,
    EdamamService,
    SnackBarService
  ],
  bootstrap: [AppComponent],
  entryComponents: [MealUpdateDialogComponent],
})
export class AppModule { }
