import {EdamamMeasure} from '../../models/edamam.model';
import {NgModule, Pipe, PipeTransform} from '@angular/core';
import { Measures } from 'edamam-api';

@Pipe({
  name: 'edamamMeasurement'
})
export class EdamamMeasurePipe implements PipeTransform {
  constructor() {}
  transform(measure: EdamamMeasure, amount?: number): string {
    const allMeasures = Object.keys(Measures);
    if (measure && allMeasures.some(m => m.toLowerCase() === measure.label.toLowerCase())) {
      const m = allMeasures.find(m => m.toLowerCase() === measure.label.toLowerCase());
      if (amount != undefined) {
        if (amount > 1) {
          const knownExceptions = ['whole']
          if (knownExceptions.some(exception => exception == m)) {
            return m;
          } else {
            return `${m}s`;
          }
        }
      } else {
        return `${m}(s)`;
      }
    } else {
      return measure.label + '(s)';
    }
  }
}
@NgModule({
  declarations: [
    EdamamMeasurePipe
  ],
  exports: [EdamamMeasurePipe]
})
export class EdamamMeasurePipeModule { }
