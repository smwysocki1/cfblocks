import { Component, ViewChild, Input, EventEmitter, Output, ViewChildren, QueryList } from "@angular/core";
import {ResourceMenu} from "../resources.component";
import { MatExpansionPanel } from "@angular/material/expansion";

@Component({
  selector: 'resource-menu',
  templateUrl: './resource-menu.component.html',
  styles: [ `
    ::ng-deep .mat-expansion-panel {
      -webkit-box-shadow: none!important;
      -moz-box-shadow: none!important;
      box-shadow: none!important;
    }
    ::ng-deep .mat-expansion-panel-body {
      padding: 0!important;
    }
  `]
})
export class ResourceMenuComponent {
  @Input() menu: ResourceMenu;
  @ViewChild('expansionPanel', {static: false}) expansionPanel: MatExpansionPanel;
  @ViewChildren('subMenus') subMenus: QueryList<ResourceMenuComponent>;
  @Output() goTo: EventEmitter<string> = new EventEmitter<string>();
  opened = false;
  constructor(){}
  ngOnInit(){

  }
  goToLink() {
    this.open();
    this.goTo.emit(this.link);
  }
  toggle() {
    if(this.opened) {
      this.close();
    } else {
      this.open();
    }
  }
  open() {
    this.opened = true;
    if (this.expansionPanel)
      this.expansionPanel.open();
  }
  close() {
    this.collapseChildren();
    this.opened = false;
    if (this.expansionPanel)
      this.expansionPanel.close();
  }
  collapseChildren() {
    if (this.subMenus && this.subMenus.length > 0) {
      this.subMenus.toArray().forEach(subMenu =>{
        subMenu.close();
      });
    }
  }
  get link(): string {
    const label = this.menu ? this.menu.label : '';
    return (label.split(" ").map(l => l.toLocaleLowerCase()).join('-'));
  }
}
