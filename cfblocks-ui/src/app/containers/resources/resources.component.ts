import { Component, ViewChild } from "@angular/core";
import {MatDrawer} from '@angular/material/sidenav';

@Component({
  selector: 'resources',
  templateUrl: './resources.component.html',
  styles: [ `
    .sidenav-content {
      margin-top: 45px;
      padding-left: 15px;
      padding-right: 15px;
    }
    .example-sidenav {
      padding: 20px;
    }
    .side-nav-bar-container {
      margin-top: 60px;
    }
  `]
})
export class ResourcesComponent {
  @ViewChild('drawer', {static: false}) drawer: MatDrawer;
  initialLevel: number = 1;
  menus: ResourceMenu[] = [{
      label: 'Getting Started',
      link: '#getting-started',
      paragraphs: [`
        Welcome to Essential Meal Planning. Here you will learn how to eat healthy and plan for success when building
        a well ballanced meal plan. We will look at what foods consist of, which nutrients are important to the body 
        (and why) as well as which nutrients should be avoided.
      `],
      subMenus: []
    }, {
      label: 'Healthy Eating',
      link: '#healthy-eating',
      paragraphs: [],
      subMenus: [{
        label: 'What is In Your Food',
        link: '#what-is-in-your-food',
        paragraphs: [],
        subMenus: []
      },{
        label: 'Macro Nutrients',
        link: '#macro-nutrients',
        paragraphs: [],
        subMenus: [{
          label: 'Calories',
          link: '#Calories',
          paragraphs: [],
          subMenus: []
        },{
          label: 'Carbohydrates',
          link: '#macro-nutrients',
          paragraphs: [],
          subMenus: []
        },{
          label: 'Fats',
          link: '#fats',
          paragraphs: [],
          subMenus: []
        },{
          label: 'Protein',
          link: '#protein',
          paragraphs: [],
          subMenus: []
        }]
      },{
        label: 'Micro Nutrients',
        link: '#micro-nutrients',
        paragraphs: [],
        subMenus: []
      }]
    }, {
      label: 'Nutrition Plans',
      link: '#nutrition-plans',
      paragraphs: [],
      subMenus: [{
        label: 'General Calorie Counting',
        link: '#general-calorie-counting',
        paragraphs: [],
        subMenus: []
      },{
        label: 'Zone Diet',
        link: '#what-is-in-your-food',
        paragraphs: [],
        subMenus: []
      },{
        label: 'Keto',
        link: '#what-is-in-your-food',
        paragraphs: [],
        subMenus: []
      },{
        label: 'Vegitarian',
        link: '#what-is-in-your-food',
        paragraphs: [],
        subMenus: []
      },{
        label: 'Vegan',
        link: '#what-is-in-your-food',
        paragraphs: [],
        subMenus: []
      }]
  }, {
    label: 'Tutorial',
    link: '#tutorial',
    paragraphs: [],
    subMenus: [{
      label: 'Managing Your Account',
      link: '#managing-your-account',
      paragraphs: [],
      subMenus: []
    },{
      label: 'Search For Food',
      link: '#search-for-food',
      paragraphs: [],
      subMenus: []
    },{
      label: 'Add To Your Meal Plan',
      link: '#add-to-your-meal-plan',
      paragraphs: [],
      subMenus: []
    },{
      label: 'Manage Meals In Calendar',
      link: '#manage-meals-in-calendar',
      paragraphs: [],
      subMenus: []
    },{
      label: 'Grocery List',
      link: '#grocery-list',
      paragraphs: [],
      subMenus: []
    }]
  }];
  constructor(){}
  goTo(link: string) {
    this.scroll(link);
  }
  scroll(id) {
    let el = document.getElementById(id);
    if (el)
      el.scrollIntoView();
  }
  ngOnInit(){

  }
  get isDrawerOpen(): boolean { return this.drawer.opened; };
}
export class ResourceMenu {
  label: string;
  link: string;
  subMenus: ResourceMenu[];
  paragraphs: string[]
}
