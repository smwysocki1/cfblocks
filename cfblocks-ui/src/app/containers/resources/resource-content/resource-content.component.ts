import { Component, Input } from "@angular/core";
import {ResourceMenu} from "../resources.component";

@Component({
  selector: 'resource-content',
  templateUrl: './resource-content.component.html',
  styles: [ `
    markdown > p {
      padding-left: 3rem!important;
    }
  `]
})
export class ResourceContentComponent {
  @Input() menu: ResourceMenu;
  @Input() level: number;
  get heading(): string {
    return "#".repeat(this.level) + " " + (this.menu ? this.menu.label : '');
  }
  constructor(){}
  ngOnInit(){

  }
  get nextLevel(): number {
    return !!this.level ? this.level + 1 : 1;
  }
}
