import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../services/login.service';
import {User, UserSession} from '../../../models/user.model';
import {Router} from '@angular/router';

@Component({
  selector: 'main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent {
  constructor(){}
}
