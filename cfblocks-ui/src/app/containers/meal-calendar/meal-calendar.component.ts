import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../services/login.service';
import {User} from '../../../models/user.model';
import { Meal } from '../../../models/meal.module';
import { MealService } from '../../../services/meal.service';
import {Subject} from 'rxjs';
import { Router } from '@angular/router';
import { MealType } from '../../../models/edamam.model';
import { SnackBarService } from '../../../services/snackbar.service';
@Component({
  templateUrl: './meal-calendar.html'
})
export class MealCalendarComponent implements OnInit {
  user: User = new User();
  view: string = 'Monthly Meal Plan';
  mealUpdates: Subject<Date[]> = new Subject<Date[]>();
  constructor(private loginService: LoginService, private ms: MealService, private router: Router, private sb: SnackBarService) { }

  ngOnInit() {
    this.user = this.loginService.getUser();
    this.loginService.getUserSessionUpdates.subscribe(update => {
      this.user = update.user;
    });
  }
  getFormatedName() {
    return this.loginService.getFormatedName();
  }
  async searchMeal(event: Meal) {
    this.router.navigate([`/food-finder`], {queryParams: {q: event.label}});
  }
  async modifyMeal(event: Meal) {
    try {
      await this.ms.modifyMeal(this.loginService.getUser(), event).toPromise();
      this.mealUpdates.next([event.eatenDate]);
    } catch(error) {
      this.sb.error(error);
    }
  }
  async removeMeal(event: Meal) {
    try {
      await this.ms.removeMeal(event).toPromise();
      this.mealUpdates.next([event.eatenDate]);
    } catch(error) {
      this.sb.error(error);
    }
  }
  async updateMealType(meal: Meal, type: MealType) {
    try{
      await this.ms.updateMealType(meal, type).toPromise();
      this.mealUpdates.next([meal.eatenDate]);
    } catch(error) {
      this.sb.error(error);
    }
  }
  async duplicateMeal(event: {meal: Meal, date: Date}) {
    try {
      await this.ms.duplicateMeal(this.loginService.getUser(), event.meal, event.date).toPromise();
      this.mealUpdates.next([event.date]);
    } catch(error) {
      this.sb.error(error);
    }
  }
}
