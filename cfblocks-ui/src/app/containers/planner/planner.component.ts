import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../../services/login.service';
import {ValidationService} from '../../../services/validation.service';
import {UserSession, User} from '../../../models/user.model';
import {Router} from '@angular/router';
declare var $: any;
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');
import {CalendarDay} from '../../component/calendar/calendar.model';
import {CalendarService} from '../../component/calendar/calendar.service';

@Component({
  selector: 'planner',
  templateUrl: './planner.component.html',
  styles: [`
    .calendar-day {
      display: inline-block;
      width: 20vw;
      height: 20vw;
    }
  `]
})
export class PlannerComponent implements OnInit {
  selectedDate: Date = moment().toDate();
  calendarStartDate: Date;
  showCalendar = false;
  calendarDisplayCount = 10;
  calendarDisplay = [];
  constructor(private cs: CalendarService) {}
  ngOnInit() {
    const daysBack = (this.calendarDisplayCount - this.calendarDisplayCount % 2) / 2;
    this.updateCalendarStartDate(moment(this.selectedDate).subtract(daysBack, 'days').toDate());
  }
  toggleCalendar() {
    this.showCalendar = !this.showCalendar;
  }
  isSelected(date: Date) {
    return this.cs.isSameDay(this.selectedDate, date);
  }
  updateCalendarStartDate(date: Date) {
    this.calendarStartDate = date;
    this.calendarDisplay = [];
    const calendarDisplay = [];
    for (let i = 0; i < this.calendarDisplayCount; i++) {
      const day = new CalendarDay(moment(this.calendarStartDate).add(i, 'days').toDate());
      calendarDisplay.push(day);
    }
    this.calendarDisplay = calendarDisplay;
  }
  pageLeft() {
    this.updateCalendarStartDate(moment(this.calendarStartDate).subtract(this.calendarDisplayCount, 'days').toDate());
  }
  pageRight() {
    this.updateCalendarStartDate(moment(this.calendarStartDate).add(this.calendarDisplayCount, 'days').toDate());
  }
  selectDay(day: CalendarDay) {
    if (this.showCalendar && day) {
      this.selectedDate = day.date;
    }
  }
}
