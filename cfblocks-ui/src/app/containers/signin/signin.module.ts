import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SigninComponent} from './signin.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    SigninComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    MatCardModule
  ],
  exports: [SigninComponent],
  providers: []
})
export class SigninModule { }
