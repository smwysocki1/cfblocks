import {Component, OnInit, ViewChild} from '@angular/core';
import {LoginService} from '../../../services/login.service';
import {User, UserSession} from '../../../models/user.model';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment-timezone';
moment.tz.setDefault('Etc/UTC');
import { EdamamMeasure } from '../../../models/edamam.model';
import { MealService } from '../../../services/meal.service';
import { Meal, MealCalendar } from '../../../models/meal.module';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatExpansionPanel } from '@angular/material/expansion';
import { SnackBarService } from '../../../services/snackbar.service';

@Component({
  selector: 'grocery-list',
  templateUrl: './grocery-list.component.html',
})
export class GroceryListComponent {
  @ViewChild('dateRangeEP', {static: false}) dateRangeEP : MatExpansionPanel;
  dateRangeEPexpanded: boolean = false;
  @ViewChild('editableDateRangeEP', {static: false}) editableDateRangeEP : MatExpansionPanel;
  editableDateRangeEPexpanded: boolean = true;
  @ViewChild('groceryListEP', {static: false}) groceryListEP : MatExpansionPanel;
  groceryListEPexpanded: boolean = false;
  helpText: string = `A grocery list can be automatically generated once your meal plan has been completed. ` + 
    `Select the date range you wish to shop for, and the list will be generated below. With this list, ` +
    `you may use the checkboxes to mark items as you add them to your cart or use "Control + P" to print ` +
    `out your grocery list to take with you.`;
  // _startDate: Date = moment().startOf('day').toDate();
  // _endDate: Date = moment().endOf('day').toDate();
  groceryItems: Grocery[] = [];
  groceryFG: FormGroup = this.fb.group({
    startDate: [moment().startOf('day').toDate()],
    endDate: [moment().endOf('day').toDate()],
    groceryList: this.fb.group({})
  });
  editingDateRange:boolean = true;
  get startDate() {return this.groceryFG.get('startDate'); };
  get endDate() {return this.groceryFG.get('endDate'); };
  get groceryList() { return this.groceryFG.get('groceryList') as FormGroup; };
  constructor(private route: ActivatedRoute, private mealService: MealService, private ls: LoginService, private fb: FormBuilder, private sb: SnackBarService){}
  async ngOnInit() {
    // this.startEditingDateRange();
    // await this.generateGroceryList();
    this.route.queryParams.subscribe(async params => {
      if (params) {
        let start = this.startDate.value;
        let end = this.endDate.value;
        if (params.startDate) {
          start = moment(params.startDate).startOf('day').toDate();
        } 
        if (params.endDate) {
          end = moment(params.endDate).endOf('day').toDate();
        }
        this.startDate.patchValue(start);
        this.endDate.patchValue(end);
        await this.generateGroceryList();
      }
    });
  }
  async onSubmit() {
    await this.generateGroceryList();
  }
  toggleDateRangeEdit() {
    this.editingDateRange = !this.editingDateRange;
    if(this.editingDateRange) {
      this.startEditingDateRange();
    } else {
      this.stopEditingDateRange();
    }
  }
  startEditingDateRange() {
    if (!this.editingDateRange) {
      this.editingDateRange = true;
    }
    // this.dateRangeEP.close();
    // this.groceryListEP.close();
    // this.editableDateRangeEP.open();
    this.dateRangeEPexpanded = false;
    this.groceryListEPexpanded = false;
    this.editableDateRangeEPexpanded = true;
  }
  stopEditingDateRange(showResults?: boolean) {
    if (this.editingDateRange) {
      this.editingDateRange = false;
    }
    // this.editableDateRangeEP.close();
    this.editableDateRangeEPexpanded = false;
    if (showResults) {
      // this.groceryListEP.open();
      this.groceryListEPexpanded = true;
    } else {
      // this.groceryListEP.close();
      this.groceryListEPexpanded = false;
    }
    // this.dateRangeEP.open();
    this.dateRangeEPexpanded = true;
  }
  async generateGroceryList(): Promise<Grocery[]> {
    try {
      this.stopEditingDateRange(true);
      const mealCalendars: MealCalendar[] = await this.mealService.getMealCalendarByDateRange(this.ls.getUser(),this.startDate.value, this.endDate.value).toPromise();
      const groceryList: Grocery[] = [];
      mealCalendars.forEach((mealCalendar: MealCalendar) => {
        if (moment(this.startDate.value).isSameOrBefore(mealCalendar.date) && moment(this.endDate.value).isSameOrAfter(mealCalendar.date)) {
          mealCalendar.meals.forEach((meal:Meal) => {
            let existingItem = groceryList.find((item: Grocery) => item.foodId === meal.foodId && item.measurement.uri === meal.measure.uri && ((meal.qualifier && item.qualifier && meal.qualifier.uri === item.qualifier.uri) || (!meal.qualifier && !item.qualifier)));
            if (existingItem) {
              existingItem.quantity = existingItem.quantity + meal.eatenAmount;
            } else {
              groceryList.push({
                label: meal.label,
                foodId: meal.foodId,
                quantity: meal.eatenAmount,
                measurement: meal.measure,
                qualifier: meal.qualifier
              });
            }
          });
        } // else skip
      });
      this.patchGroceryList(groceryList);
      return groceryList;
    } catch(error) {
      this.startEditingDateRange();
      console.error(error);
      this.sb.error(error);
      return [] as Grocery[];
    }
  }
  patchGroceryList(groceryList: Grocery[]): void {
    this.groceryItems = groceryList;
    Object.keys(this.groceryList.controls).forEach((control: string) => {
      this.groceryList.removeControl(control);
    });
    groceryList.forEach((groceryItem: Grocery) => {
      const formControlName = this.getFormControlName(groceryItem);
      this.groceryList.addControl(formControlName, new FormControl(false));
    });
  }
  getFormControlName(groceryItem: Grocery) {
    const itemsToJoin = [groceryItem.foodId,groceryItem.measurement.label];
    if (groceryItem.qualifier) {
      itemsToJoin.push(groceryItem.qualifier.label)
    }
    const controlName = itemsToJoin.join('-');
    return controlName;
  }
  // print() {
  //   window.print();
  // }
}
export class Grocery {
  label: string;
  foodId: string;
  quantity: number;
  measurement: EdamamMeasure;
  qualifier?: EdamamMeasure;
}
