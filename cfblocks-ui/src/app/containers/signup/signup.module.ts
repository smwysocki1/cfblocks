import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SignupComponent} from './signup.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    SignupComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    MatCardModule
  ],
  exports: [SignupComponent],
  providers: []
})
export class SignupModule { }
