import { Measures } from 'edamam-api';

export class EdamamParserQueryParams {
  UPC?: string;
  query?: string;
  'nutrition-type'?: EdamamNutritionType;
  health?: HealthLabel;
  calories?: number;
  page?: number;
  category?: EdamamFoodCategory;
  categoryLabel?: EdamamCategoryLabel;
}

export enum EdamamNutritionType {
  'LOGGING' = 'logging',
  'DEFAULT' = 'null',
  '' = 'null'
}

export class EdamamFoodRes {
  text: string;
  parsed: { 
    food?: EdamamFood,
    recipe?: EdamamRecipe
  }[];
  hints: EdamamFoodHint[];
  _links: {
    next: {
      title: string,
      href: string
    }
  }
}

export class EdamamFoodHint {
  food?: EdamamFood;
  recipe?: EdamamRecipe;
  measures: EdamamMeasure[];
}

export class EdamamMeasure {
  uri: string;
  label: string;
}

export class EdamamFood {
  "foodId": string;
  "label": string;
  "nutrients": {
    "ENERC_KCAL": number,
    "PROCNT": number,
    "FAT": number,
    "CHOCDF": number,
    "FIBTG": number
  };
  "category": EdamamFoodCategory;
  "categoryLabel": EdamamCategoryLabel;
  "foodContentsLabel": string;
  "image": string
}

export class EdamamRecipe {

}

export enum EdamamFoodCategory {
  'Generic foods',
  'Packaged foods',
  'Generic meals',
  'Fast foods'
}

export enum EdamamCategoryLabel {
  'food',
  'recipe'
}

export class EdamamNutritionRes {
  uri: string;
  calories: 0;
  totalWeight: 0;
  dietLabels: DietLabel[];
  healthLabels: HealthLabel[];
  cautions: [];
  totalNutrients: {};
  totalDaily: {};
  totalNutrientsKCal: {
  ENERC_KCAL: KcalLabel;
  PROCNT_KCAL: KcalLabel;
  FAT_KCAL: KcalLabel;
  CHOCDF_KCAL: KcalLabel
  }
}
export class KcalLabel {
  label: string;
  quantity: number;
  unit: string;
}
export enum DietLabel {
"BALANCED",
"HIGH_FIBER",
"HIGH_PROTEIN",
"LOW_CARB",
"LOW_FAT",
"LOW_SODIUM"
}
export enum HealthLabel {
  "LOW_SUGAR",
  "VEGAN",
  "VEGETARIAN",
  "PESCATARIAN",
  "PALEO",
  "SPECIFIC_CARBS",
  "DAIRY_FREE",
  "GLUTEN_FREE",
  "WHEAT_FREE",
  "EGG_FREE",
  "MILK_FREE",
  "PEANUT_FREE",
  "TREE_NUT_FREE",
  "SOY_FREE",
  "FISH_FREE",
  "SHELLFISH_FREE",
  "PORK_FREE",
  "RED_MEAT_FREE",
  "CRUSTACEAN_FREE",
  "CELERY_FREE",
  "MUSTARD_FREE",
  "SESAME_FREE",
  "LUPINE_FREE",
  "MOLLUSK_FREE",
  "ALCOHOL_FREE",
  "NO_OIL_ADDED",
  "NO_SUGAR_ADDED",
  "SULPHITE_FREE",
  "FODMAP_FREE",
  "KOSHER"
}

export enum EdamamCaution {
  'SULFITES'
}

export enum MealType {
  'BREAKFAST' = 'Breakfast',
  'LUNCH' = 'Lunch',
  'DINNER' = 'Dinner',
  'SNACK' = 'Snack',
  'TEATIME' = 'Teatime'
}
export enum DishType {
  'Alcohol-cocktail',
  'Biscuits and cookies',
  'Bread',
  'Cereals',
  'Condiments and sauces',
  'Drinks',
  'Desserts',
  'Egg',
  'Main course',
  'Omelet',
  'Pancake',
  'Preps',
  'Preserve',
  'Salad',
  'Sandwiches',
  'Soup',
  'Starter'
}
export enum CusineType {
  'American',
	'Asian',
	'British',
	'Caribbean',
	'Central Europe',
	'Chinese',
	'Eastern Europe',
	'French',
	'Indian',
	'Italian',
	'Japanese',
	'Kosher',
	'Mediterranean',
	'Mexican',
	'Middle Eastern',
	'Nordic',
	'South American',
	'South East Asian'
}
export class EdamamNutrientsRes {
  uri: string;
  calories: number;
  totalWeight: number;
  dietLabels: any[ ];
  healthLabels: HealthLabel[];
  cautions: EdamamCaution[];
  totalNutrients : {
    ENERC_KCAL: KcalLabel,
    FAT: KcalLabel,
    FASAT: KcalLabel,
    FAMS: KcalLabel
    FAPU: KcalLabel,
    CHOCDF: KcalLabel,
    FIBTG: KcalLabel,
    SUGAR: KcalLabel,
    PROCNT: KcalLabel,
    CHOLE: KcalLabel,
    NA: KcalLabel,
    CA: KcalLabel,
    MG: KcalLabel,
    K: KcalLabel,
    FE: KcalLabel,
    ZN: KcalLabel,
    P: KcalLabel,
    VITA_RAE: KcalLabel,
    VITC: KcalLabel,
    THIA: KcalLabel,
    RIBF: KcalLabel,
    NIA: KcalLabel,
    VITB6A: KcalLabel,
    FOLDFE: KcalLabel,
    FOLFD: KcalLabel,
    FOLAC: KcalLabel,
    VITB12: KcalLabel,
    VITD: KcalLabel,
    TOCPHA: KcalLabel,
    VITK1: KcalLabel,
    WATER: KcalLabel
  };
  totalDaily: {
    ENERC_KCAL: KcalLabel,
    FAT: KcalLabel,
    FASAT: KcalLabel,
    CHOCDF: KcalLabel,
    FIBTG: KcalLabel,
    PROCNT: KcalLabel,
    CHOLE: KcalLabel,
    NA: KcalLabel,
    CA: KcalLabel,
    MG: KcalLabel,
    K: KcalLabel,
    FE: KcalLabel,
    ZN: KcalLabel,
    P: KcalLabel,
    VITA_RAE: KcalLabel,
    VITC: KcalLabel,
    THIA: KcalLabel,
    RIBF: KcalLabel,
    NIA: KcalLabel,
    VITB6A: KcalLabel,
    FOLDFE: KcalLabel,
    VITB12: KcalLabel,
    VITD: KcalLabel,
    TOCPHA: KcalLabel,
    VITK1: KcalLabel
  };
  ingredients: [
    {
      parsed: [
        {
          quantity: number,
          measure: string,
          food: string,
          foodId: string,
          weight: number,
          retainedWeight: string,
          measureURI: string,
          status: string
        }
      ]
    }
  ];
}